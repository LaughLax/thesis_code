package gasNetwork;

public class Compressor extends Branch {
	double pressureRatioMin;
	double pressureRatioMax;
	double powerMin;
	double powerMax;
	
	double k1;
	double k2;
	double alpha;
	
	double heatRate0;
	double heatRate1;
	double heatRate2;
	
	public Compressor() {}
	
	public Compressor(Branch branch) {
		this.id = branch.id;
		this.fromNode = branch.fromNode;
		this.toNode = branch.toNode;
	}
}
