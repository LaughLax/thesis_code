package gasNetwork;

import ilog.concert.*;
import ilog.cplex.*;

public class SLPGasSubproblem {

	IloCplex cp;
	GasVars vars;
	EvalPoint ePoint;
	GasSystemData sys;

	IloRange[][] nodeMismatch;
	//IloRange[] gasStorageLims;
	//IloRange[] pressureStepLimits;

	IloRange[][] pipeFlowLinearized;

	IloRange[][] compressorFlowLinearized;
	IloRange[][] compressorPressureLowerLimits;
	IloRange[][] compressorPressureUpperLimits;

	public SLPGasSubproblem(IloCplex cplex, GasSystemData systemData, GasVars variables, SLPParams params, EvalPoint ep) {

		cp = cplex;
		vars = variables;
		ePoint = ep;
		sys = systemData;
		initalizeVariables(params);

		try {

			IloNumExpr[][] nodeMis = new IloNumExpr[sys.numNodes][sys.numPeriods];

			for (int t=0; t<sys.numPeriods; t++) {
				double demandMultiplier = sys.periods[t].demandMultiplier;
				for (int n=0; n<sys.numNodes; n++) {
					Node node = sys.nodes[n];


					nodeMis[n][t] = cp.diff(cp.sum(vars.loadViolMinus[n][t], vars.nodeStorageFlux[n][t]), vars.loadViolPlus[n][t]);
					//nodeMis[n][t] = cp.diff(vars.loadViolMinus[n][t], vars.loadViolPlus[n][t]);

					if (node instanceof Supplier)
						nodeMis[n][t] = cp.sum(nodeMis[n][t], vars.nodeSupply[((Supplier) node).supplierID][t]);

					nodeMis[n][t] = cp.sum(nodeMis[n][t], -node.demand*demandMultiplier);

					//gasStorageLims[n] = cp.range(node.storageMin, arg1, arg2)

					//pressureStepLimits[n] = cp.range(ep.nodePressure[n]-ep.pressureStepMax[n], vars.nodePressure[n], ep.nodePressure[n]+ep.pressureStepMax[n]);

					vars.nodePressure[n][t].setLB(Math.max(ep.nodePressure[n][t]-ep.pressureStepMax[n][t], node.pressureMin));
					vars.nodePressure[n][t].setUB(Math.min(ep.nodePressure[n][t]+ep.pressureStepMax[n][t], node.pressureMax));
				}

				for (int p=0; p<sys.numPipes; p++) {

					Pipe pipe = sys.pipes[p];
					pipeFlowLinearized[p][t] = buildPipeFlow(pipe, ep, t);
					nodeMis[pipe.fromNode][t] = cp.diff(nodeMis[pipe.fromNode][t], vars.branchFlow[p][t]);
					nodeMis[pipe.toNode][t] = cp.sum(nodeMis[pipe.toNode][t], vars.branchFlow[p][t]);
				}

				for (int c=0; c<sys.numCompressors; c++) {
					Compressor comp = sys.compressors[c];
					int from = comp.fromNode;
					int to = comp.toNode;

					compressorFlowLinearized[c][t] = buildCompressorFlow(sys.compressors[c], ep, t);
					nodeMis[from][t] = cp.diff(nodeMis[from][t], vars.branchFlow[comp.id][t]);
					nodeMis[to][t] = cp.sum(nodeMis[to][t], vars.branchFlow[comp.id][t]);

					compressorPressureLowerLimits[c][t] = cp.ge(cp.diff(vars.nodePressure[to][t], cp.prod(vars.nodePressure[from][t], comp.pressureRatioMin)), 0.0);
					compressorPressureUpperLimits[c][t] = cp.le(cp.diff(vars.nodePressure[to][t], cp.prod(vars.nodePressure[from][t], comp.pressureRatioMax)), 0.0);
				}
				for (int n=0; n<sys.numNodes; n++) {
					nodeMismatch[n][t] = cp.eq(nodeMis[n][t], 0.0);
				}
			}

			addToModel();
		}
		catch(IloException e) { e.printStackTrace(); }
	}

	private void initalizeVariables(SLPParams params) {
		nodeMismatch = new IloRange[sys.numNodes][sys.numPeriods];
		//gasStorageLims = new IloRange[sys.numNodes];
		//pressureStepLimits = new IloRange[sys.numNodes];

		pipeFlowLinearized = new IloRange[sys.numPipes][sys.numPeriods];

		compressorFlowLinearized = new IloRange[sys.numCompressors][sys.numPeriods];
		compressorPressureLowerLimits = new IloRange[sys.numCompressors][sys.numPeriods];
		compressorPressureUpperLimits = new IloRange[sys.numCompressors][sys.numPeriods];		
	}

	private void addToModel() {
		try {
			cp.add(vars.objFunction);

			for (int n=0; n<sys.numNodes; n++)
				cp.add(nodeMismatch[n]);
			//cp.add(gasStorageLims);
			//cp.add(pressureStepLimits);

			for (int p=0; p<sys.numPipes; p++)
				cp.add(pipeFlowLinearized[p]);

			for (int c=0; c<sys.numCompressors; c++) {
				cp.add(compressorFlowLinearized[c]);
				cp.add(compressorPressureLowerLimits[c]);
				cp.add(compressorPressureUpperLimits[c]);
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public int run() {
		try{
			if (cp.solve()) {
				vars.getRunResult();

				cp.output().printf("Subproblem %d solution status = %s ($%.2f, $%.2f penalties)\n",
						ePoint.iteration,
						cp.getStatus(),
						cp.getObjValue(),
						vars.penaltyValue);

				return 1;
			}
			else {
				System.err.println("Subproblem failed to solve. Quitting.");
				return 0;
			}
		}
		catch(IloException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public void addPenalties() {
		try {
			cp.remove(cp.getObjective());
			cp.add(vars.objFunction);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}

	public void updateEvalPoint(EvalPoint ep) {
		try {
			for (int p=0; p<sys.numPipes; p++) {
				cp.remove(pipeFlowLinearized[p]);
				for (int t=0; t<sys.numPeriods; t++)
					pipeFlowLinearized[p][t] = buildPipeFlow(sys.pipes[p], ep, t);
				cp.add(pipeFlowLinearized[p]);
			}

			for (int c=0; c<sys.numCompressors; c++) {
				cp.remove(compressorFlowLinearized[c]);
				for (int t=0; t<sys.numPeriods; t++)
					compressorFlowLinearized[c][t] = buildCompressorFlow(sys.compressors[c], ep, t);
				cp.add(compressorFlowLinearized[c]);				
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public void updateStepLims(SLPParams params, EvalPoint ep) {

		try {
			for (int t=0; t<sys.numPeriods; t++) {
				double f = 0.0;
				for (int n=0; n<sys.numNodes; n++) {
					f += params.mismatchPen * vars.nodeMismatch[n][t];
				}
				double gamma = (vars.penaltyValue + f) / (vars.costValue + f);
				gamma = Math.min(gamma, 1);
				double beta = -params.a * Math.log(gamma) + params.b;
				double delta = 1.0 - Math.floor(10.0*gamma)/10.0;
				double alpha = beta/delta;

				for (int n=0; n<sys.numNodes; n++) {
					ep.pressureStepMax[n][t] = alpha * (sys.nodes[n].pressureMax - sys.nodes[n].pressureMin) / Math.pow(ep.iteration, beta);
					vars.nodePressure[n][t].setLB(Math.max(ep.nodePressure[n][t]-ep.pressureStepMax[n][t], sys.nodes[n].pressureMin));
					vars.nodePressure[n][t].setUB(Math.min(ep.nodePressure[n][t]+ep.pressureStepMax[n][t], sys.nodes[n].pressureMax));
				}
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public void tinyStepLims(EvalPoint ep) {
		try {
			for (int t=0; t<sys.numPeriods; t++) {
				for (int n=0; n<vars.nodePressure.length; n++) {
					vars.nodePressure[n][t].setLB(Math.max(ep.nodePressure[n][t]-1e-10, sys.nodes[n].pressureMin));
					vars.nodePressure[n][t].setUB(Math.min(ep.nodePressure[n][t]+1e-10, sys.nodes[n].pressureMax));
				}
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public int runWithNoPenalties(EvalPoint ep) {
		try {
			ep.update(sys, cp, vars);
			updateEvalPoint(ep);

			cp.remove(vars.objFunction);
			cp.addMinimize(vars.costExpression);

			tinyStepLims(ep);

			if (cp.solve()) {
				System.out.println("Final run successful");
				return 1;
			}
			else {
				System.out.println("Final run failed - " + cp.getStatus());
				return 0;
			}
		}
		catch (IloException e) {
			e.printStackTrace();
			return 0;
		}
	}

	private IloRange buildPipeFlow(Pipe pipe, EvalPoint ep, int period) {
		int from = pipe.fromNode;
		int to = pipe.toNode;

		double piFrom = ep.nodePressure[from][period];
		double piTo = ep.nodePressure[to][period];

		double f_xy = Math.signum(piFrom - piTo)
				* pipe.flowConstant
				* Math.sqrt(Math.abs(Math.pow(piFrom, 2.0) - Math.pow(piTo, 2.0)));

		double fPartial_piFrom;
		if (Math.abs(piFrom - piTo) < 1e-4)
			fPartial_piFrom = 1e10;
		else
			fPartial_piFrom = pipe.flowConstant * piFrom / Math.sqrt(Math.abs(Math.pow(piFrom, 2.0) - Math.pow(piTo, 2.0)));

		double fPartial_piTo = fPartial_piFrom * -piTo/piFrom;

		try {
			return cp.eq(cp.sum(cp.sum(cp.prod(fPartial_piFrom, cp.diff(vars.nodePressure[from][period], piFrom)),
					cp.prod(fPartial_piTo, cp.diff(vars.nodePressure[to][period], piTo)),
					cp.negative(vars.branchFlow[pipe.id][period])),
					f_xy),0.0);
		}
		catch (IloException e) {
			e.printStackTrace();
			return null;
		}
	}

	private IloRange buildCompressorFlow(Compressor comp, EvalPoint ep, int period) {
		int from = comp.fromNode;
		int to = comp.toNode;

		double piFrom = ep.nodePressure[from][period];
		double piTo = ep.nodePressure[to][period];
		double compPower = ep.compressorPower[comp.id - sys.numPipes][period];

		double f_xyz = compPower / (comp.k1 * Math.pow(piTo / piFrom, comp.alpha) - comp.k2);

		double fPartial_piFrom = comp.alpha * compPower * comp.k1 * Math.pow(piTo, comp.alpha) / Math.pow(piFrom, comp.alpha + 1) /
				Math.pow(comp.k1 * Math.pow(piTo / piFrom, comp.alpha) - comp.k2, 2.0);

		double fPartial_piTo = -comp.alpha * compPower * comp.k1 * Math.pow(piTo, comp.alpha-1) / Math.pow(piFrom, comp.alpha) /
				Math.pow(comp.k1 * Math.pow(piTo / piFrom, comp.alpha) - comp.k2, 2.0);

		double fPartial_compPower = f_xyz / compPower;

		try {
			return cp.eq(cp.sum(cp.sum(cp.prod(fPartial_piFrom, cp.diff(vars.nodePressure[from][period], piFrom)),
					cp.prod(fPartial_piTo, cp.diff(vars.nodePressure[to][period], piTo)),
					cp.prod(fPartial_compPower, cp.diff(vars.compressorPower[comp.id - sys.numPipes][period], compPower)),
					cp.negative(vars.branchFlow[comp.id][period])),
					f_xyz),0.0);
		}
		catch (IloException e) {
			e.printStackTrace();
			return null;
		}
	}
}
