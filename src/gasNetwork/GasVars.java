package gasNetwork;

import ilog.concert.*;
import ilog.cplex.*;

public class GasVars {

	IloCplex cp;
	GasSystemData sys;

	IloNumVar[][] nodePressure;
	IloNumVar[][] nodeStorage;
	IloNumVar[][] nodeStorageFlux;
	IloNumVar[][] nodeSupply;

	IloNumVar[][] compressorPower;
	IloNumVar[][] branchFlow;

	IloNumVar[][] loadViolPlus;
	IloNumVar[][] loadViolMinus;

	IloObjective objFunction;
	IloNumExpr costExpression;
	IloNumExpr penaltyExpression;

	double[][] nodeMismatch;
	double totalNodeMismatch = 0.0;
	double maxNodeMismatch = 0.0;

	double costValue = 0.0;
	double penaltyValue = 0.0;

	public GasVars(IloCplex cplex) {
		cp = cplex;
	}

	public void initialize(GasSystemData systemData, SLPParams par) {
		try {
			sys = systemData;

			costExpression = cp.numExpr();
			penaltyExpression = cp.numExpr();

			initNodeVars(par);
			initSupplierVars();
			initBranchVars();

			objFunction = cp.minimize(cp.sum(costExpression, penaltyExpression));

			nodeMismatch = new double[sys.numNodes][sys.numPeriods];
		}
		catch(IloException e) { e.printStackTrace(); }
	}

	private void initNodeVars(SLPParams par) {
		nodePressure = new IloNumVar[sys.numNodes][sys.numPeriods];
		nodeStorage = new IloNumVar[sys.numNodes][sys.numPeriods];

		nodeStorageFlux = new IloNumVar[sys.numNodes][sys.numPeriods];

		loadViolPlus = new IloNumVar[sys.numNodes][sys.numPeriods];
		loadViolMinus = new IloNumVar[sys.numNodes][sys.numPeriods];

		try {
			for (int n=0; n<sys.numNodes; n++) {
				nodePressure[n] = cp.numVarArray(sys.numPeriods, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
				nodeStorage[n] = cp.numVarArray(sys.numPeriods, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
				nodeStorageFlux[n] = cp.numVarArray(sys.numPeriods, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

				loadViolPlus[n] = cp.numVarArray(sys.numPeriods, 0.0, Double.POSITIVE_INFINITY);
				loadViolMinus[n] = cp.numVarArray(sys.numPeriods, 0.0, Double.POSITIVE_INFINITY);

				for (int t=0; t<sys.numPeriods; t++) {
					nodePressure[n][t].setLB(sys.nodes[n].pressureMin);
					nodePressure[n][t].setUB(sys.nodes[n].pressureMax);
					nodeStorage[n][t].setLB(sys.nodes[n].storageMin);
					nodeStorage[n][t].setUB(sys.nodes[n].storageMax);

					// TODO: Put in a better storage flux limit (within SLPGasSubproblem)
					nodeStorageFlux[n][t].setLB(sys.nodes[n].storageMin - sys.nodes[n].storageMax);
					nodeStorageFlux[n][t].setUB(sys.nodes[n].storageMax - sys.nodes[n].storageMin);

					penaltyExpression = cp.sum(penaltyExpression,
							cp.prod(par.loadShedPenalty, cp.sum(loadViolPlus[n][t], loadViolMinus[n][t])));
				}
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	private void initSupplierVars() {

		nodeSupply = new IloNumVar[sys.numSuppliers][sys.numPeriods];

		try {
			for (int s=0; s<sys.numSuppliers; s++) {
				nodeSupply[s] = cp.numVarArray(sys.numPeriods, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

				for (int t=0; t<sys.numPeriods; t++) {
					nodeSupply[s][t].setLB(sys.suppliers[s].supplyMin);
					nodeSupply[s][t].setUB(sys.suppliers[s].supplyMax);
				}
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	private void initBranchVars() {

		compressorPower = new IloNumVar[sys.numCompressors][sys.numPeriods]; 
		branchFlow = new IloNumVar[sys.numBranches][sys.numPeriods];

		try {
			for (int b=0; b<sys.numBranches; b++) {
				branchFlow[b] = cp.numVarArray(sys.numPeriods, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
				if (b>=sys.numPipes) {
					int c = b - sys.numPipes;
					compressorPower[c] = cp.numVarArray(sys.numPeriods, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

					for (int t=0; t<sys.numPeriods; t++) {
						compressorPower[c][t].setLB(sys.compressors[c].powerMin);
						compressorPower[c][t].setUB(sys.compressors[c].powerMax);

						// TODO: Build a better objective function
						costExpression = cp.sum(costExpression, compressorPower[c][t]);
					}
				}
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public void addToModel() {
		try {
			for (int n=0; n<sys.numNodes; n++) {
				cp.add(nodePressure[n]);
				cp.add(nodeStorage[n]);
				cp.add(nodeStorageFlux[n]);
				cp.add(nodeSupply[n]);

				cp.add(loadViolPlus[n]);
				cp.add(loadViolMinus[n]);
			}

			for (int b=0; b<sys.numBranches; b++) {
				cp.add(branchFlow[b]);
				if (b>=sys.numPipes)
					cp.add(compressorPower[b-sys.numPipes]);
			}
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}

	public void getNodeMismatch() {
		try {
			totalNodeMismatch = 0.0;
			maxNodeMismatch = 0.0;

			for (int t=0; t<sys.numPeriods; t++) {
				for (int n=0; n<sys.numNodes; n++) {
					Node node = sys.nodes[n];

					nodeMismatch[n][t] = cp.getValue(nodeStorageFlux[n][t])
							- node.demand * sys.periods[t].demandMultiplier;
					
					if (sys.nodes[n].getClass() == Supplier.class) {
						Supplier supplier = (Supplier) node;
						nodeMismatch[n][t] += cp.getValue(nodeSupply[supplier.supplierID][t]);
					}

					for (int b : node.branchesFrom) {
						Branch branch = sys.branches[b];

						double piFrom = cp.getValue(nodePressure[n][t]);
						double piTo = cp.getValue(nodePressure[branch.toNode][t]);
						double flow = 0.0;

						if (branch instanceof Pipe) {
							flow = Math.signum(piFrom - piTo)
									* ((Pipe) branch).flowConstant
									* Math.sqrt(Math.abs(Math.pow(piFrom, 2.0) - Math.pow(piTo, 2.0)));
						} else if (branch instanceof Compressor) {
							Compressor comp = (Compressor) branch;
							double compPower = cp.getValue(compressorPower[comp.id-sys.numPipes][t]);

							flow = compPower / (comp.k1 * Math.pow(piTo / piFrom, comp.alpha) - comp.k2);
						}

						nodeMismatch[n][t] -= flow;
					}
					for (int b : node.branchesTo) {
						Branch branch = sys.branches[b];

						double piFrom = cp.getValue(nodePressure[branch.fromNode][t]);
						double piTo = cp.getValue(nodePressure[n][t]);
						double flow = 0.0;

						if (branch instanceof Pipe) {
							flow = Math.signum(piFrom - piTo)
									* ((Pipe) branch).flowConstant
									* Math.sqrt(Math.abs(Math.pow(piFrom, 2.0) - Math.pow(piTo, 2.0)));
						} else if (branch instanceof Compressor) {
							Compressor comp = (Compressor) branch;
							double compPower = cp.getValue(compressorPower[comp.id-sys.numPipes][t]);

							flow = compPower / (comp.k1 * Math.pow(piTo / piFrom, comp.alpha) - comp.k2);
						}

						nodeMismatch[n][t] += flow;
					}

					nodeMismatch[n][t] = Math.abs(nodeMismatch[n][t]);
					maxNodeMismatch = Math.max(maxNodeMismatch, nodeMismatch[n][t]);
					totalNodeMismatch += nodeMismatch[n][t];
				}
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public void getCost() {
		try { costValue = cp.getValue(costExpression); }
		catch (IloException e) { e.printStackTrace(); }
	}

	public void getPenalties() {
		try {
			if (cp.getObjective() == objFunction)
				penaltyValue = cp.getValue(penaltyExpression);
			else
				penaltyValue = 0.0;
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	public void getRunResult() {
		getNodeMismatch();
		getCost();
		getPenalties();
	}

	public void increasePenalties(SLPParams par, double multiplier) {
		par.increasePenalties(multiplier);

		try {
			cp.remove(objFunction);

			penaltyExpression = cp.numExpr();

			for (int t=0; t<sys.numPeriods; t++) {
				for (int n=0; n<sys.numNodes; n++) {
					penaltyExpression = cp.sum(penaltyExpression,
							cp.prod(par.loadShedPenalty, cp.sum(loadViolPlus[n][t], loadViolMinus[n][t])));
				}
			}

			objFunction = cp.minimize(cp.sum(costExpression, penaltyExpression));
			cp.add(objFunction);

		}
		catch (IloException e) { e.printStackTrace(); }
	}
}
