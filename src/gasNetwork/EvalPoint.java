package gasNetwork;

import java.util.Random;

import ilog.concert.*;
import ilog.cplex.*;

public class EvalPoint {
	
	final static int FLAT_START = 0;
	final static int RANDOM_START = 1;

	double[][] nodePressure;
	double[][] nodeSupply; 
	double[][] compressorPower;

	double[][] pressureStepMax;
	int iteration;
	
	public EvalPoint(GasSystemData sys, SLPParams params, int type) {

		nodePressure = new double[sys.numNodes][sys.numPeriods];
		nodeSupply = new double[sys.numSuppliers][sys.numPeriods];
		compressorPower = new double[sys.numCompressors][sys.numPeriods];

		pressureStepMax = new double[sys.numNodes][sys.numPeriods];

		switch (type) {
		case FLAT_START:
			prepFlatStart(sys);
			break;
		case RANDOM_START:
			prepRandomStart(sys);
		}

		//calculateFlows(sys);

		iteration = 0;
	}

	private void prepFlatStart(GasSystemData sys) {
		for (int n=0; n<sys.numNodes; n++) {
			Node node = sys.nodes[n];
			nodePressure[n][0] = (node.pressureMin + node.pressureMax) / 2.0;
			if (node instanceof Supplier) {
				Supplier supp = (Supplier) node;
				nodeSupply[supp.supplierID][0] = (supp.supplyMin + supp.supplyMax) / 2.0;
			}
			pressureStepMax[n][0] = 1e3;
		}
		for (int c=0; c < sys.numCompressors; c++)
			compressorPower[c][0] = (sys.compressors[c].powerMin + sys.compressors[c].powerMax) / 2.0;

		for (int t=1; t<sys.numPeriods; t++) {
			for (int n=0; n<sys.numNodes; n++) {
				nodePressure[n][t] = nodePressure[n][0];
				pressureStepMax[n][t] = pressureStepMax[n][0];
			}
			for (int s=0; s<sys.numSuppliers; s++)
				nodeSupply[s][t] = nodeSupply[s][0];
			for (int c=0; c<sys.numCompressors; c++)
				compressorPower[c][t] = compressorPower[c][0];
		}
	}

	private void prepRandomStart(GasSystemData sys) {
		Random r = new Random();

		for (int t=0; t<sys.numPeriods; t++) {
			for (int n=0; n < sys.numNodes; n++) {
				Node node = sys.nodes[n];
				nodePressure[n][t] = (node.pressureMax - node.pressureMin) * r.nextDouble() + node.pressureMin;
				if (node instanceof Supplier) {
					Supplier supp = (Supplier) node;
					nodeSupply[supp.supplierID][t] = (supp.supplyMax - supp.supplyMin) * r.nextDouble() + supp.supplyMin;
				}
				pressureStepMax[n][t] = 1e3;
			}
			for (int c=0; c < sys.numCompressors; c++) {
				Compressor comp = sys.compressors[c];
				compressorPower[c][t] = (comp.powerMax - comp.powerMin)  * r.nextDouble() + comp.powerMin;
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void calculateFlows(GasSystemData sys) {
		for (int t=0; t<sys.numPeriods; t++) {
			for (int b=0; b < sys.numBranches; b++) {
				Branch branch = sys.branches[b];
				int from = branch.fromNode;
				int to = branch.toNode;

				double flow;
				double piFrom = nodePressure[from][t];
				double piTo = nodePressure[to][t];

				if (b<sys.numPipes) {
					flow = Math.signum(piFrom - piTo)
							* ((Pipe) branch).flowConstant
							* Math.sqrt(Math.abs(Math.pow(piFrom, 2.0) - Math.pow(piTo, 2.0)));
				} else {
					Compressor comp = (Compressor) branch;
					double compPower = compressorPower[comp.id-sys.numPipes][t];

					flow = compPower / (comp.k1 * Math.pow(piTo / piFrom, comp.alpha) - comp.k2);
				}
			}
		}
	}

	void update(GasSystemData sys, IloCplex cp, GasVars vars) {
		try {
			for (int n=0; n<sys.numNodes; n++)
				nodePressure[n] = cp.getValues(vars.nodePressure[n]);
			for (int s=0; s<sys.numSuppliers; s++)
				nodeSupply[s] = cp.getValues(vars.nodeSupply[s]);
			for (int c=0; c<sys.numCompressors; c++)
				compressorPower[c] = cp.getValues(vars.compressorPower[c]);

			iteration++;
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
}
