package gasNetwork;
/*Copyright (c) 2013, Mostafa Sahraei-Ardakani, Arizona State University 
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
�	Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class GasSystemData {
	int numPeriods;
	
	int numNodes;
	int numSuppliers;
	
	int numBranches;
	int numPipes;
	int numCompressors;
	
	Period[] periods;
	
	Node[] nodes;
	Supplier[] suppliers;
	
	Branch[] branches;
	Pipe[] pipes;
	Compressor[] compressors;
	
	public void initialize(){
		nodes = new Node[numNodes];
		suppliers = new Supplier[numSuppliers];
		
		branches = new Branch[numBranches];
		pipes = new Pipe[numPipes];
		compressors = new Compressor[numCompressors];
		
		periods = new Period[numPeriods];
		
		for (int i=0; i<numNodes; i++) {
			nodes[i] = new Node();
		}
		for (int i=0; i<numBranches; i++) {
			if (i<numPipes) { 
				pipes[i] = new Pipe();
				branches[i] = pipes[i];
			}
			else {
				compressors[i-numPipes] = new Compressor();
				branches[i] = compressors[i-numPipes];
			}
		}
		for (int i=0; i<numPeriods; i++) {
			periods[i] = new Period();
		}
	}

	public void loadParams(String folderName) {
		
		String rootDr=(System.getProperty("user.dir")+"\\gas cases\\"+folderName+"\\");
		
		try {
			InputStream myFile= new FileInputStream(rootDr+"modelParameters.txt");
			Scanner modelParam = new Scanner(myFile);
			
			numNodes=modelParam.nextInt();
			numSuppliers = modelParam.nextInt();
			
			numPipes=modelParam.nextInt();
			numCompressors=modelParam.nextInt();
			numBranches = numPipes + numCompressors;
			
			numPeriods=modelParam.nextInt();
			
			System.out.println("Model Parameters Was Successfully Read.");
			modelParam.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error loading Parameters file!");
			e.printStackTrace();
		}
	}	 

	public void loadData(String folderName) {

		String rootDr=(System.getProperty("user.dir")+"\\gas cases\\"+folderName+"\\");

		if (numPeriods > 1) {
			readPeriods(rootDr);
		} else {
			periods[0].hour = 0;
			periods[0].electricityCost = 40;
			periods[0].demandMultiplier = 1.0;
		}

		readNodes(rootDr);
		readSuppliers(rootDr);

		readPipes(rootDr);
		readCompressors(rootDr);

		System.out.println();
		System.out.println("Data Load Finished.");
	}

	private void readPeriods(String rootDr) {
		try {
			InputStream myFile = new FileInputStream(rootDr+"periods.txt");
			Scanner periodData = new Scanner(myFile);
			for (int t=0; t<numPeriods; ++t){
				periods[t].hour = periodData.nextInt();
				periods[t].electricityCost = periodData.nextDouble();
				periods[t].demandMultiplier = periodData.nextDouble();
			}

			System.out.println("Period Data Was Successfully Read.");
			periodData.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error loading period file! " + e);
			e.printStackTrace();
		}
	}
	
	private void readNodes(String rootDr) {
		try {
			InputStream myFile = new FileInputStream(rootDr+"nodes.txt");
			Scanner myScanner = new Scanner(myFile);
			for(int n=0; n<numNodes; n++){
				nodes[n].nodeID = myScanner.nextInt()-1;
				nodes[n].demand = myScanner.nextDouble();
				nodes[n].pressureMin = myScanner.nextDouble();
				nodes[n].pressureMax = myScanner.nextDouble();
				nodes[n].storageMin = myScanner.nextDouble();
				nodes[n].storageMax = myScanner.nextDouble();
			}	

			System.out.println("Node Data Was Successfully Read.");
			myScanner.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Node File!");
			e.printStackTrace();
		}
	}

	private void readSuppliers(String rootDr) {
		try {
			InputStream myFile = new FileInputStream(rootDr+"suppliers.txt");
			Scanner myScanner = new Scanner(myFile);
			for(int s=0; s<numSuppliers; s++){
				int suppID = myScanner.nextInt() - 1;
				int node = myScanner.nextInt()-1;
				suppliers[s] = new Supplier(nodes[node]);
				suppliers[s].supplierID = suppID;
				suppliers[s].supplyMin = myScanner.nextDouble();
				suppliers[s].supplyMax = myScanner.nextDouble();
			
				nodes[node] = suppliers[s];
			}	

			System.out.println("Supplier Data Was Successfully Read.");
			myScanner.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Supplier File!");
			e.printStackTrace();
		}
	}

	private void readPipes(String rootDr) {
		try {
			InputStream myFile = new FileInputStream(rootDr+"pipes.txt");
			Scanner myScanner = new Scanner(myFile);

			for(int p=0; p<numPipes; p++){
				pipes[p].id = myScanner.nextInt() - 1;
				
				pipes[p].fromNode = myScanner.nextInt() - 1;
				nodes[pipes[p].fromNode].addBranchFrom(p);
				pipes[p].toNode = myScanner.nextInt() - 1;
				nodes[pipes[p].toNode].addBranchTo(p);
				
				pipes[p].flowConstant = myScanner.nextDouble();
			}

			System.out.println("Pipe Data Was Successfully Read.");
			myScanner.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Pipes File!");
			e.printStackTrace();
		}
	}
	
	private void readCompressors(String rootDr) {
		try {
			InputStream myFile = new FileInputStream(rootDr+"compressors.txt");
			Scanner myScanner = new Scanner(myFile);

			for(int c=0; c<numCompressors; c++){
				compressors[c].id = myScanner.nextInt() - 1 + numPipes;
				
				compressors[c].fromNode = myScanner.nextInt() - 1;
				nodes[compressors[c].fromNode].addBranchFrom(c+numPipes);
				compressors[c].toNode = myScanner.nextInt() - 1;
				nodes[compressors[c].toNode].addBranchTo(c+numPipes);
				
				compressors[c].pressureRatioMin = myScanner.nextDouble();
				compressors[c].pressureRatioMax = myScanner.nextDouble();
				compressors[c].powerMin = myScanner.nextDouble();
				compressors[c].powerMax = myScanner.nextDouble();
				
				compressors[c].k1 = myScanner.nextDouble();
				compressors[c].k2 = myScanner.nextDouble();
				compressors[c].alpha = myScanner.nextDouble();
				
				compressors[c].heatRate0 = myScanner.nextDouble();
				compressors[c].heatRate1 = myScanner.nextDouble();
				compressors[c].heatRate2 = myScanner.nextDouble();
			}

			System.out.println("Compressor Data Was Successfully Read.");
			myScanner.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Compressor File!");
			e.printStackTrace();
		}
	}

}