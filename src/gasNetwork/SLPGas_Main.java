package gasNetwork;

import java.io.File;
import java.io.PrintStream;

import ilog.concert.*;
import ilog.cplex.*;

public class SLPGas_Main {
	
	// TODO: Double-check solution flow
	
	// TODO: Add time periods!
	
	static GasSystemData sys;
	static SLPParams params;
	static GasResults results;
	
	final static String CASE_ID = "7node";
	final static double PENALTY_TOLERANCE = 1.0;
	
	public static void main(String[] args) {
		try {
			long startTime = System.nanoTime();
			
			//setOutputToFile();
			
			loadSystemData(CASE_ID);
			
			// Initialize problem
			IloCplex cplex = new IloCplex();
			setCplexParams(cplex);
			
			EvalPoint evalPoint = new EvalPoint(sys, params, EvalPoint.FLAT_START);
			
			GasVars vars = new GasVars(cplex);
			vars.initialize(sys, params);
			
			SLPGasSubproblem sp = new SLPGasSubproblem(cplex, sys, vars, params, evalPoint);
			
			int result = sp.run();
			
			while (result > 0 && isStopCriteriaMet(cplex, vars, evalPoint) == 0) {
				if (result > 0) {				

					//returnSolution(cplex, vars, sp);
					evalPoint.update(sys, cplex, vars);
					sp.updateEvalPoint(evalPoint);
					sp.updateStepLims(params, evalPoint);
					//vars.increasePenalties(params, 1.10);
					//returnEPSolution(evalPoint);

					result = sp.run();
				}
			}
			
			if (result > 0) {
				long elapsedTime = System.nanoTime() - startTime;
				double penal = vars.penaltyValue;
				if (penal > 0.0) {
					System.out.printf("$%.2f worth of penalties active. Recalculating without penalties.\n", penal);
					returnSolution(cplex, vars, sp);
					if (sp.runWithNoPenalties(evalPoint) > 0);
						returnSolution(cplex, vars, sp);
						System.out.printf("Elapsed time: %.2e seconds\n", elapsedTime / 1e9);
				}
				else {
					returnSolution(cplex, vars, sp);
					System.out.printf("Elapsed time: %.2e seconds\n", elapsedTime / 1e9);
				}
			}
			
			cplex.end();
		}
		catch (IloException e) {
			
		}
	}
	
	private static void loadSystemData(String folder) {
		sys = new GasSystemData();
		sys.loadParams(folder);
		sys.initialize();
		sys.loadData(folder);
		
		//sys.adjustLineLimits(Double.POSITIVE_INFINITY);
		//sys.adjustQLimits(100.0);
		
		params = new SLPParams();

		// If desired, adjust computational parameters here. Example:
		//params.vPenMultiplier = 25.0;
		//params.a = 0.1;
		params.maxIterations = 50;
		params.loadNodeMismatch = 1e-8;
		params.loadTotalMismatch = 1e-6;
		
		params.computePenalties(sys);
	}
	
	static void setCplexParams(IloCplex cp) {
		try {
			cp.setParam(IloCplex.IntParam.SimDisplay, 0);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	private static int isStopCriteriaMet(IloCplex cp, GasVars vars, EvalPoint ep) {

		if (ep.iteration >= params.maxIterations) {
			System.out.println("Max iterations reached.");
			return 2;
		}
		else {
			
			if (vars.maxNodeMismatch <= params.loadNodeMismatch && vars.penaltyValue <= PENALTY_TOLERANCE) {
				System.out.println("Individual node mismatches within tolerance.");
				return 1;
			}
			else if (vars.totalNodeMismatch <= params.loadTotalMismatch && vars.penaltyValue <= PENALTY_TOLERANCE) {
				System.out.println("Total node mismatches within tolerance.");
				return 1;
			}
			else {
				//System.out.printf("Stopping conditions not met in iteration %d. Continuing.\n", ep.iter);
				return 0;
			}
		}
	}

	private static void returnSolution(IloCplex cp, GasVars vars, SLPGasSubproblem sp) {
		
		results = new GasResults(cp, vars, sp, sys);
		results.printSummary();
		if (sys.numNodes <= 100)
			results.printResults();		
	}

	@SuppressWarnings("unused")
	private static void setOutputToFile() {
		try {
			System.setOut(new PrintStream(new File("output.txt")));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
