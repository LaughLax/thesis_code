modelParameters.txt
numNodes	numSuppliers	numPipes	numCompressors	numPeriods

periods.txt
hour	electricityCost

nodes.txt
id	demand	pressureMin	pressureMax	storageMin	storageMax

suppliers.txt
id	node	supplyMin	supplyMax

pipes.txt
id	fromNode	toNode	flowConstant

compressors.txt
id	fromNode	toNode	pressureRatioMin	pressureRatioMax	powerMin	powerMax	k1	k2	alpha	heatRate0	heatRate1	heatRate2
