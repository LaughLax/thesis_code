package gasNetwork;

public class Supplier extends Node {
	
	int supplierID;
	double supplyMin;
	double supplyMax;
	
	public Supplier(Node n) {
		this.nodeID = n.nodeID;
		this.pressureMin = n.pressureMin;
		this.pressureMax = n.pressureMax;
		this.storageMin = n.storageMin;
		this.storageMax = n.storageMax;
		this.demand = n.demand;
	}
}
