package gasNetwork;

public class Node {
	
	int nodeID;
	double demand;

	double pressureMin;
	double pressureMax;
	double storageMin = 0.0;
	double storageMax = 0.0;
	
	int[] branchesFrom = {};
	int[] branchesTo = {};
	
	public void addBranchFrom(int branch) {
		int len = branchesFrom.length;
		int[] newArray = new int[len+1];
		for (int i=0; i<len; i++) {
			newArray[i] = branchesFrom[i];
		}
		newArray[len] = branch;
		branchesFrom = newArray;
	}
	
	public void addBranchTo(int branch) {
		int len = branchesTo.length;
		int[] newArray = new int[len+1];
		for (int i=0; i<len; i++) {
			newArray[i] = branchesTo[i];
		}
		newArray[len] = branch;
		branchesTo = newArray;
	}
}