package gasNetwork;

import ilog.cplex.*;
import ilog.concert.*;

public class GasResults {
	
	GasSystemData sys;
	GasVars vars;
	
	double objectiveValue;

	double[][] nodePressure;
	//double[] nodeStorage;
	double[][] nodeStorageFlux;
	double[][] nodeSupply;

	double[][] violPlus;
	double[][] violMinus;

	double[][] branchFlow;
	double[][] compressorPower;

	public GasResults(IloCplex cp, GasVars variables, SLPGasSubproblem sp, GasSystemData sysData) {
		try {
			objectiveValue = cp.getObjValue();
			
			vars = variables;
			sys = sysData;

			nodePressure = new double[sys.numNodes][sys.numPeriods];
			nodeStorageFlux = new double[sys.numNodes][sys.numPeriods];

			violPlus = new double[sys.numNodes][sys.numPeriods];
			violMinus = new double[sys.numNodes][sys.numPeriods];

			nodeSupply = new double[sys.numSuppliers][sys.numPeriods];

			branchFlow = new double[sys.numBranches][sys.numPeriods];
			compressorPower = new double[sys.numCompressors][sys.numPeriods];

			for (int n=0; n<sys.numNodes; n++) {
				nodePressure[n] = cp.getValues(vars.nodePressure[n]);
				//nodeStorage = cp.getValues(vars.nodeStorage);
				nodeStorageFlux[n] = cp.getValues(vars.nodeStorageFlux[n]);

				violPlus[n] = cp.getValues(vars.loadViolPlus[n]);
				violMinus[n] = cp.getValues(vars.loadViolMinus[n]);
			}

			for (int s=0; s<sys.numSuppliers; s++) {
				nodeSupply[s] = cp.getValues(vars.nodeSupply[s]);
			}

			for (int b=0; b<sys.numBranches; b++) {
				branchFlow[b] = cp.getValues(vars.branchFlow[b]);
				if (b>=sys.numPipes)
					compressorPower[b-sys.numPipes] = cp.getValues(vars.compressorPower[b-sys.numPipes]);
			}
		}
		catch (IloException e) { e.printStackTrace(); }
	}

	void printSummary() {
		System.out.printf("Objective function value: $%.2f\n", objectiveValue);
	}
	
	void printResults() {
		// TODO: Gas_Results.printResults()
		if (sys.numNodes <= 100) {

			if (sys.numPeriods>5)
				System.out.println("More than five time periods. Only showing results for t<5.");
			for (int t=0; t<Math.min(sys.numPeriods, 5); t++) {
				System.out.printf("Node\t| Pressure\t| Demand\t| Viol-/+\t| Supply\t(t=%d)\n", t);
				for (int n=0; n<sys.numNodes; n++) {
					System.out.printf("%d\t| %.0f/%.1f/%.0f\t| %.0f\t\t| %.1f/%.1f\t| ",
							n,
							sys.nodes[n].pressureMin, nodePressure[n][t], sys.nodes[n].pressureMax,
							sys.nodes[n].demand*sys.periods[t].demandMultiplier,
							violMinus[n][0], violPlus[n][t]);
					if (sys.nodes[n] instanceof Supplier)
						System.out.printf("%.1f", nodeSupply[((Supplier) sys.nodes[n]).supplierID][t]);
					System.out.println();
				}

				System.out.println("\nBranch\t| From\t| To\t| Flow");
				for (int b=0; b<sys.numBranches; b++) {
					Branch branch = sys.branches[b];
					System.out.printf("%d\t| %d\t| %d\t| %.2f\t| ", b, branch.fromNode, branch.toNode, branchFlow[b][t]);
					if (b<sys.numPipes) {
						System.out.printf("%.1f\n", ((Pipe) branch).flowConstant);
					} else {
						System.out.printf("%.1f\n", compressorPower[b-sys.numPipes][t]);
					}
				}
				System.out.println();
			}
		} else
			System.out.println("You've got a pretty big system there.");
	}
}
