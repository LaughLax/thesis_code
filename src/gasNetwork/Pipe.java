package gasNetwork;

public class Pipe extends Branch {
	double flowConstant;
	
	public Pipe() {}
	
	public Pipe(Branch branch) {
		this.id = branch.id;
		this.fromNode = branch.fromNode;
		this.toNode = branch.toNode;
	}
}
