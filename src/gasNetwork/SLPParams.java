package gasNetwork;

public class SLPParams {
	
	// TODO: Double check what you want to do here
	
	int maxIterations = 20;
	double a = 0.25;
	double b = 1.5;
	double r = 0.9;
	
	double loadPenMultiplier = 15.0;
	
	double loadShedPenalty;
	double mismatchPen = 0.2;
	
	double loadNodeMismatch = 1.0e-3;
	double loadTotalMismatch = 5*loadNodeMismatch;
	
	public void computePenalties(GasSystemData sys) {
		double max_c_g_l = 0;
		for (int p=0; p<sys.numPeriods; p++) {
			max_c_g_l = Math.max(max_c_g_l, sys.periods[p].electricityCost);
		}
		if (max_c_g_l == 0)
			max_c_g_l = 400;
		loadShedPenalty = loadPenMultiplier*max_c_g_l;
		System.out.printf("Load shed penalty: %.1f\n", loadShedPenalty);
	}
	
	public void increasePenalties(double multiplier) {
		loadShedPenalty *= multiplier;
	}
	
}
