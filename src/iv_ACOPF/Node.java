package iv_ACOPF;

public class Node {
	
	int id;                                 // node id (from 0 to numNodes-1)
	double pDemand;                            // demand during peak hour
	double qDemand;
	double vMin;
	double vMax;
	double g;
	double b;
	boolean hasGen = false;

}