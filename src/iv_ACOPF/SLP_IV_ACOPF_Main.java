package iv_ACOPF;

import java.io.File;
import java.io.PrintStream;

import ilog.concert.*;
import ilog.cplex.*;

public class SLP_IV_ACOPF_Main {
	
	static SystemData sys;
	static SLP_Params params;
	static IV_ACOPF_Results results;
	
	final static String CASE_ID = "IEEECase14";
	final static double PENALTY_TOLERANCE = 0.0;
	
	public static void main(String[] args) {
		try {
			long startTime = System.nanoTime();
			
			//setOutputToFile();
			
			loadSystemData(CASE_ID);
			
			// Initialize problem
			IloCplex cplex = new IloCplex();
			setCplexParams(cplex);
			
			EvalPoint evalPoint = new EvalPoint(sys, params, EvalPoint.FLAT_START);
			
			IV_ACOPF_Vars vars = new IV_ACOPF_Vars(cplex);
			vars.initialize(sys, params);
			
			SLP_Subproblem sp = new SLP_Subproblem(cplex, sys, vars, params, evalPoint);
			
			int result = sp.run();
			
			while (result > 0 && isStopCriteriaMet(cplex, vars, evalPoint) == 0) {
				if (result > 0) {				

					returnMidSolution(cplex, vars);
					evalPoint.update(cplex, vars);
					sp.updateEvalPoint(sys, evalPoint);
					sp.updateFlowConstraints(cplex, sys, params, evalPoint);
					evalPoint.checkVUpperBounds(sp, sys);
					evalPoint.checkIUpperBounds(sp, sys);
					sp.updateStepLims(sys, params, evalPoint);
					vars.increasePenalties(params, 1.10);
					//returnEPSolution(evalPoint);

					result = sp.run();
				}
			}
			
			if (result > 0) {
				long elapsedTime = System.nanoTime() - startTime;
				double penal = vars.penaltyValue;
				if (penal > 0.0) {
					System.out.printf("$%.2f worth of penalties active. Recalculating without penalties.\n", penal);
					returnSolution(cplex, vars, sp);
					if (sp.runWithNoPenalties(sys, evalPoint) > 0);
						returnSolution(cplex, vars, sp);
						System.out.printf("Elapsed time: %.2e seconds\n", elapsedTime / 1e9);
				}
				else {
					returnSolution(cplex, vars, sp);
					System.out.printf("Elapsed time: %.2e seconds\n", elapsedTime / 1e9);
				}
			}
			
			cplex.end();
		}
		catch (IloException e) {
			
		}
	}
	
	private static void loadSystemData(String folder) {
		sys = new SystemData();
		sys.loadParams(folder);
		sys.initialize();
		sys.loadData(folder);
		
		//sys.adjustLineLimits(Double.POSITIVE_INFINITY);
		//sys.adjustQLimits(100.0);
		
		params = new SLP_Params();

		// If desired, adjust computational parameters here. Example:
		//params.vPenMultiplier = 20.0;
		//params.a = 0.1;
		//params.maxIterations = 5;
		//params.linearSegments = 200;
		params.p_n_mismatch = 1e-8;
		params.q_n_mismatch = 1e-8;
		params.p_total_mismatch = 1e-6;
		params.q_total_mismatch = 1e-6;
		
		params.linearizeGenCost(sys);
		params.computePenalties(sys);
	}
	
	static void setCplexParams(IloCplex cp) {
		try {
			cp.setParam(IloCplex.IntParam.SimDisplay, 0);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	private static int isStopCriteriaMet(IloCplex cp, IV_ACOPF_Vars vars, EvalPoint ep) {

		if (ep.iteration >= params.maxIterations) {
			System.out.println("Max iterations reached.");
			return 2;
		}
		else {
			double delta_p = 0;
			double delta_q = 0;
		
			double maxDeltaPN = 0.0;
			double maxDeltaQN = 0.0;
		
			//System.out.printf("dPn\t| dQn\t| dP\t| dQ\n");
			for (int b=0; b<sys.numNodes; b++) {
				maxDeltaPN = Math.max(vars.deltaP[b], maxDeltaPN);
				maxDeltaQN = Math.max(vars.deltaQ[b], maxDeltaQN);
				
				delta_p += vars.deltaP[b];
				delta_q += vars.deltaQ[b];
				/*System.out.printf("%.2f\t| %.2f\t| %.2f\t| %.2f\n",
						vars.deltaP[b],
						vars.deltaQ[b],
						delta_p,
						delta_q);*/
			}
			
			if (Math.max(maxDeltaPN, maxDeltaQN) <= params.p_n_mismatch && vars.penaltyValue <= PENALTY_TOLERANCE) {
				System.out.println("Individual bus mismatches within tolerance.");
				return 1;
			}
			else if (Math.max(delta_p, delta_q) <= params.p_total_mismatch && vars.penaltyValue <= PENALTY_TOLERANCE) {
				System.out.println("Total bus mismatches within tolerance.");
				return 1;
			}
			else {
				//System.out.printf("Stopping conditions not met in iteration %d. Continuing.\n", ep.iter);
				return 0;
			}
		}
	}

	private static void returnSolution(IloCplex cp, IV_ACOPF_Vars vars, SLP_Subproblem sp) {
		
		results = new IV_ACOPF_Results(cp, vars, sp, sys);
		results.printSummary();
		if (sys.numNodes <= 100)
			results.printResults();		
	}

	private static void returnMidSolution(IloCplex cp, IV_ACOPF_Vars vars) {
		
		try {
			double[] v_d = cp.getValues(vars.vReal);
			double[] v_q = cp.getValues(vars.vImag);
			double[] in_d = cp.getValues(vars.iNodeReal);
			double[] in_q = cp.getValues(vars.iNodeImag);
			double maxPMismatch = 0.0;
			double maxQMismatch = 0.0;
			double totalPMismatch = 0.0;
			double totalQMismatch = 0.0;
			
			System.out.println("Node\t| Voltage mag.\t| Voltage angle\t| P Actual\t| P Mismatch\t| Q Actual\t| Q Mismatch");
			for (int b=0; b<sys.numNodes; b++) {
				double v_mag = Math.sqrt(Math.pow(v_d[b],2.0) + Math.pow(v_q[b],2.0));
				double v_ang = Math.atan2(v_q[b], v_d[b]) * 180 / Math.PI;
				//double in_mag = Math.sqrt(Math.pow(in_d[b],2.0) + Math.pow(in_q[b],2.0));
				//double in_ang = Math.atan2(in_q[b], in_d[b]) * 180 / Math.PI;
				double pMis = vars.pMismatch[b];
				double qMis = vars.qMismatch[b];
				System.out.printf("%d\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\n",
						b,
						v_mag,
						v_ang,
						vars.pActual[b],
						vars.pMismatch[b],
						vars.qActual[b],
						vars.qMismatch[b]);
				maxPMismatch = Math.max(maxPMismatch, pMis);
				maxQMismatch = Math.max(maxQMismatch, qMis);
				totalPMismatch += pMis;
				totalQMismatch += qMis;
			}
			System.out.printf("Max/Total P Mismatch: %.4f/%.4f\t| Max/Total Q Mismatch: %.4f/%.4f\t| Penalties: %.4f\n",
					maxPMismatch, totalPMismatch, maxQMismatch, totalQMismatch, vars.penaltyValue);
			
			/*double[] ik_d = cp.getValues(vars.ik_d);
			double[] ik_q = cp.getValues(vars.ik_q);
			
			System.out.println("\nBranch | From | To | Forward Imag | Forward Iang | Reverse Imag | Reverse Iang");
			for (int l=0; l<sys.numLine; l++) {
				double ik_mag1 = Math.sqrt(Math.pow(ik_d[l*2],2.0) + Math.pow(ik_q[l*2],2.0));
				double ik_ang1 = Math.atan2(ik_q[l*2], ik_d[l*2]);
				double ik_mag2 = Math.sqrt(Math.pow(ik_d[l*2+1],2.0) + Math.pow(ik_q[l*2+1],2.0));
				double ik_ang2 = Math.atan2(ik_q[l*2+1], ik_d[l*2+1]);
				System.out.println(l + "\t| " + sys.lines[l].fromNode + "\t| " + sys.lines[l].toNode + "\t| " + ik_mag1 + "\t| " + ik_ang1 + "\t| " + ik_mag2 + "\t| " + ik_ang2);
			}*/
		}
		catch (IloException e) {
			
		}
	}

	private static void returnMidPenalties(IloCplex cp, IV_ACOPF_Vars vars) {
		try {
			double[] vMinus = cp.getValues(vars.vViolMinus);
			double[] vPlus = cp.getValues(vars.vViolPlus);
			double[] pMinus = cp.getValues(vars.pViolMinus);
			double[] pPlus = cp.getValues(vars.pViolPlus);
			double[] qMinus = cp.getValues(vars.qViolMinus);
			double[] qPlus = cp.getValues(vars.qViolPlus);
			
			double[] vD = cp.getValues(vars.vReal);
			double[] vQ = cp.getValues(vars.vImag);
			double[] pN = vars.pActual;
			double[] qN = vars.qActual;
					
			double totalV = 0.0;
			double totalP = 0.0;
			double totalQ = 0.0;
			double totalI = 0.0;
			
			boolean vPrinted = false;
			for (int b=0; b<sys.numNodes; b++) {
				if (vMinus[b] != 0.0 ||
						vPlus[b] != 0.0 ||
						pMinus[b] != 0.0 ||
						pPlus[b] != 0.0 ||
						qMinus[b] != 0.0 ||
						qPlus[b] != 0.0) {
					if (!vPrinted) {
						System.out.println("Node\t| V (act.)\t| V-\t\t| V+\t\t| P (est.)\t| P-\t\t| P+\t\t| Q (est.)\t| Q-\t\t| Q+");
						vPrinted = true;
					}
					double vMag = Math.sqrt( Math.pow(vD[b], 2.0) + Math.pow(vQ[b], 2.0) );
					System.out.printf("%d\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\n",
							b,
							vMag,
							vMinus[b],
							vPlus[b],
							pN[b],
							pMinus[b],
							pPlus[b],
							qN[b],
							qMinus[b],
							qPlus[b]);
					totalV += vMinus[b] + vPlus[b];
					totalP += pMinus[b] + pPlus[b];
					totalQ += qMinus[b] + qPlus[b];
				}
			}
			
			double[] ik_d = cp.getValues(vars.iLineReal);
			double[] ik_q = cp.getValues(vars.iLineImag);
			double[] iPlus = cp.getValues(vars.iViolPlus);
			
			boolean iPrinted = false;
			for (int l=0; l<sys.numLines; l++) {
				if (iPlus[l*2] != 0.0 ||
						iPlus[l*2+1] != 0.0) {
					if (!iPrinted) {
						System.out.println("\nLine\t| If (act.)\t| If+\t\t| Ir (act.)\t| Ir+");
						iPrinted = true;
					}
					double ik_mag1 = Math.sqrt(Math.pow(ik_d[l*2],2.0) + Math.pow(ik_q[l*2],2.0));
					double ik_mag2 = Math.sqrt(Math.pow(ik_d[l*2+1],2.0) + Math.pow(ik_q[l*2+1],2.0));
					System.out.printf("%d\t| %.4f\t| %.4f\t| %.4f\t| %.4f\n",
							l,
							ik_mag1,
							iPlus[l*2],
							ik_mag2,
							iPlus[l*2+1]);
					totalI += iPlus[l*2] + iPlus[l*2+1];
				}
			}
			if (vPrinted || iPrinted)
				System.out.printf("Total V Viol.: %f | Total P Viol.: %f | Total Q Viol.: %f | Total I Viol.: %f | Penalties: %f\n",
						totalV, totalP, totalQ, totalI, vars.penaltyValue);
		}
		catch (IloException e) {
			
		}
	}

	private static void setOutputToFile() {
		try {
			System.setOut(new PrintStream(new File("output.txt")));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
