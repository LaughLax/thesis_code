package iv_ACOPF;

import ilog.concert.*;
import ilog.cplex.*;

public class IV_ACOPF_Vars {
	
	IloCplex cp;
	SystemData sys;
	
	IloNumVar[] vReal;
	IloNumVar[] vImag;
	IloNumVar[] iLineReal;
	IloNumVar[] iLineImag;
	IloNumVar[] iNodeReal;
	IloNumVar[] iNodeImag;
	IloNumVar[] pNodeLinear;
	
	IloNumVar[] vSquared;
	IloNumVar[] iLineSquared;
	IloNumVar[] pGen;
	IloNumVar[] qGen;

	IloNumVar[] pViolPlus;
	IloNumVar[] pViolMinus;
	IloNumVar[] qViolPlus;
	IloNumVar[] qViolMinus;
	IloNumVar[] vViolPlus;
	IloNumVar[] vViolMinus;
	IloNumVar[] iViolPlus;
	
	IloObjective objFunction;
	IloNumExpr costExpression;
	IloNumExpr penaltyExpression;
	
	double[] pActual;
	double[] qActual;
	double[] pMismatch;
	double[] qMismatch;
	double[] deltaP;
	double[] deltaQ;
	double costValue = 0.0;
	double penaltyValue = 0.0;
	
	public IV_ACOPF_Vars(IloCplex cplex) {
		cp = cplex;
	}
	
	public void initialize(SystemData systemData, SLP_Params par) {
		try {
			float inf = Float.POSITIVE_INFINITY;
			float nInf = Float.NEGATIVE_INFINITY;
			
			sys = systemData;
			
			penaltyExpression = cp.numExpr();
			
			vReal = cp.numVarArray(sys.numNodes, nInf, inf);
			vImag = cp.numVarArray(sys.numNodes, nInf, inf);
			iNodeReal = cp.numVarArray(sys.numNodes, nInf, inf);
			iNodeImag = cp.numVarArray(sys.numNodes, nInf, inf);
			
			pViolPlus = cp.numVarArray(sys.numNodes, 0.0, inf);
			pViolMinus = cp.numVarArray(sys.numNodes, 0.0, inf);
			qViolPlus = cp.numVarArray(sys.numNodes, 0.0, inf);
			qViolMinus = cp.numVarArray(sys.numNodes, 0.0, inf);
			vViolPlus = cp.numVarArray(sys.numNodes, 0.0, inf);
			vViolMinus = cp.numVarArray(sys.numNodes, 0.0, inf);
			
			for(int n=0; n<sys.numNodes; n++) {
				vReal[n].setLB(-sys.nodes[n].vMax);
				vReal[n].setUB(sys.nodes[n].vMax);
				vImag[n].setLB(-sys.nodes[n].vMax);
				vImag[n].setUB(sys.nodes[n].vMax);
				
				penaltyExpression = cp.sum(penaltyExpression,
						cp.prod(par.pPenalty, cp.sum(pViolPlus[n], pViolMinus[n])),
						cp.prod(par.qPenalty, cp.sum(qViolPlus[n], qViolMinus[n])),
						cp.prod(par.vPenalty, cp.sum(vViolPlus[n], vViolMinus[n])));
			}
			vImag[0].setLB(0.0);
			vImag[0].setUB(0.0);
			
			pGen = cp.numVarArray(sys.numGens, nInf, inf);
			qGen = cp.numVarArray(sys.numGens, nInf, inf);
			pNodeLinear = cp.numVarArray(sys.numGens*par.linearSegments, 0.0, inf);
			costExpression = cp.numExpr();
			
			for(int g=0; g<sys.numGens; g++) {
				Generator gen = sys.gens[g];
				
				for(int a=0; a<par.linearSegments; a++) {
					int ind = g*par.linearSegments+a;
					double range = gen.pMax - gen.pMin;
					pNodeLinear[ind].setUB(range/((double)par.linearSegments));
					costExpression = cp.sum(costExpression, cp.prod(gen.linearCost[a], pNodeLinear[ind]));			
				}
				
				pGen[g].setLB(gen.pMin);
				pGen[g].setUB(gen.pMax);
				
				qGen[g].setLB(gen.qMin);
				qGen[g].setUB(gen.qMax);
				
				costExpression = cp.sum(costExpression, sys.gens[g].cMin);
			}
			cp.add(pNodeLinear);
			
			iLineReal = cp.numVarArray(sys.numLines*2, nInf, inf);
			iLineImag = cp.numVarArray(sys.numLines*2, nInf, inf);			
			iViolPlus = cp.numVarArray(sys.numLines*2, 0.0, inf);
			
			for(int l=0; l<sys.numLines; l++) {
				iLineReal[l*2].setLB(-sys.lines[l].i_max);
				iLineReal[l*2].setUB(sys.lines[l].i_max);
				iLineReal[l*2+1].setLB(-sys.lines[l].i_max);
				iLineReal[l*2+1].setUB(sys.lines[l].i_max);
				iLineImag[l*2].setLB(-sys.lines[l].i_max);
				iLineImag[l*2].setUB(sys.lines[l].i_max);
				iLineImag[l*2+1].setLB(-sys.lines[l].i_max);
				iLineImag[l*2+1].setUB(sys.lines[l].i_max);
				
				penaltyExpression = cp.sum(penaltyExpression,
								   cp.prod(par.iPenalty, cp.sum(iViolPlus[l*2], iViolPlus[l*2+1])));
			}
			
			objFunction = cp.minimize(cp.sum(costExpression, penaltyExpression));
			
			vSquared = cp.numVarArray(sys.numNodes, nInf, inf);
			iLineSquared = cp.numVarArray(sys.numLines*2, nInf, inf);
						
			pActual = new double[sys.numNodes];
			qActual = new double[sys.numNodes];
			pMismatch = new double[sys.numNodes];
			qMismatch = new double[sys.numNodes];
			deltaP = new double[sys.numNodes];
			deltaQ = new double[sys.numNodes];
		}
		catch(IloException e) {
			
		}
	}
	
	public void addToModel() {
		try {
			cp.add(vReal);
			cp.add(vImag);
			cp.add(iNodeReal);
			cp.add(iNodeImag);
			cp.add(pViolPlus);
			cp.add(pViolMinus);
			cp.add(qViolPlus);
			cp.add(qViolMinus);
			cp.add(vViolPlus);
			cp.add(vViolMinus);
			
			cp.add(iLineReal);
			cp.add(iLineImag);
			cp.add(iViolPlus);

			cp.add(vSquared);
			cp.add(pGen);
			cp.add(qGen);
			cp.add(iLineSquared);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getPActual() {
		try {
			for (int b=0; b<sys.numNodes; b++) {
				pActual[b] = sys.nodes[b].pDemand
						+ cp.getValue(vReal[b]) * cp.getValue(iNodeReal[b])
						+ cp.getValue(vImag[b]) * cp.getValue(iNodeImag[b]);
			}
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
		
	public void getQActual() {
		try {
			for (int b=0; b<sys.numNodes; b++) {
				qActual[b] = sys.nodes[b].qDemand
						+ cp.getValue(vImag[b]) * cp.getValue(iNodeReal[b])
						- cp.getValue(vReal[b]) * cp.getValue(iNodeImag[b]);
			}
		}
		catch(IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getPowerMismatch() {
		try {
			double[] workingValue = new double[sys.numNodes];
			for (int g=0; g<sys.numGens; g++)
				workingValue[sys.gens[g].node] += cp.getValue(pGen[g]); 
			for (int b=0; b<sys.numNodes; b++)
				pMismatch[b] = Math.abs(pActual[b] - workingValue[b]);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getQMismatch() {
		try {
			double[] workingValue = new double[sys.numNodes];
			for (int g=0; g<sys.numGens; g++)
				workingValue[sys.gens[g].node] += cp.getValue(qGen[g]);
			for (int b=0; b<sys.numNodes; b++) {
				qMismatch[b] =  Math.abs(qActual[b] - workingValue[b]);
			}
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getDeltaP() {
		try {
			double[] pLin = new double[sys.numNodes];
			for (int g=0; g<sys.numGens; g++)
				pLin[sys.gens[g].node] += cp.getValue(pGen[g]);
			for (int b=0; b<sys.numNodes; b++) {
				double result = Math.abs(pActual[b] - pLin[b]) / Math.min(Math.abs(pActual[b]), Math.abs(pLin[b]));
				if (Double.isNaN(result) || Double.isInfinite(result))
					result = Math.abs(pActual[b] - pLin[b]);
				deltaP[b] = result;
			}
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getDeltaQ() {
		try {
			double[] qLin = new double[sys.numNodes];
			for (int g=0; g<sys.numGens; g++)
				qLin[sys.gens[g].node] += cp.getValue(qGen[g]);
			for (int b=0; b<sys.numNodes; b++) {
				double result =  Math.abs(qActual[b] - qLin[b]) / Math.min(Math.abs(qActual[b]), Math.abs(qLin[b]));
				if (Double.isNaN(result) || Double.isInfinite(result))
					result = Math.abs(qActual[b] - qLin[b]);
				deltaQ[b] = result;
			}
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getCost() {
		try {
			costValue = cp.getValue(costExpression);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getPenalties() {
		try {
			if (cp.getObjective() == objFunction)
				penaltyValue = cp.getValue(penaltyExpression);
			else
				penaltyValue = 0.0;
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void getRunResult() {
		getPActual();
		getQActual();
		getPowerMismatch();
		getQMismatch();
		getDeltaP();
		getDeltaQ();
		getCost();
		getPenalties();
	}
	
	public void increasePenalties(SLP_Params par, double multiplier) {
		par.increasePenalties(multiplier);
		
		try {
			cp.remove(objFunction);
			
			penaltyExpression = cp.numExpr();
			
			for (int n=0; n<sys.numNodes; n++) {
				penaltyExpression = cp.sum(penaltyExpression,
						   cp.prod(par.vPenalty, cp.sum(vViolPlus[n], vViolMinus[n])),
						   cp.prod(par.pPenalty, cp.sum(pViolPlus[n], pViolMinus[n])),
						   cp.prod(par.qPenalty, cp.sum(qViolPlus[n], qViolMinus[n])));
			}
			for (int l=0; l<sys.numLines; l++) {
				penaltyExpression = cp.sum(penaltyExpression,
						   cp.prod(par.iPenalty, cp.sum(iViolPlus[l*2], iViolPlus[l*2+1])));
			}
			
			objFunction = cp.minimize(cp.sum(costExpression, penaltyExpression));
			cp.add(objFunction);
			
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
}
