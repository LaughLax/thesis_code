package iv_ACOPF;

import java.util.Random;

import ilog.concert.*;
import ilog.cplex.*;

public class EvalPoint {
	
	final static int FLAT_START = 0;
	final static int UNIFORM_START = 1;
	final static int DC_WARM_START = 2;
	
	double[] vReal;
	double[] vImag;
	double[] iLineReal;
	double[] iLineImag;
	double[] iNodeReal;
	double[] iNodeImag;
	double[] vStepMax;
	int iteration;
	
	public EvalPoint(SystemData sys, SLP_Params params, int type) {
		vReal = new double[sys.numNodes];
		vImag = new double[sys.numNodes];
		iLineReal = new double[sys.numLines*2];
		iLineImag = new double[sys.numLines*2];
		iNodeReal = new double[sys.numNodes];
		iNodeImag = new double[sys.numNodes];
		vStepMax = new double[sys.numNodes];
		
		switch (type) {
		case FLAT_START:
			for (int b=0; b < sys.numNodes; b++) {
				vReal[b] = 1.0;
				vImag[b] = 0.0;
				vStepMax[b] = 2;
			}
			break;
		case UNIFORM_START:
			Random r = new Random();
			
			for (int b=0; b < sys.numNodes; b++) {
				Node node = sys.nodes[b];
				vReal[b] = (node.vMax - node.vMin) * r.nextDouble() + node.vMin;
				vImag[b] = 0.0;
				vStepMax[b] = 2;
			}
			break;
		case DC_WARM_START:
			double[] vAngle = getDCWarmStart(sys, params);
			for (int b=0; b < sys.numNodes; b++) {
				vReal[b] = Math.cos(vAngle[b]);
				vImag[b] = Math.sin(vAngle[b]);
				vStepMax[b] = 0.1;
			}
		}
		
		calculateCurrents(sys);
		
		iteration = 0;
	}
	
	private static double[] getDCWarmStart(SystemData sys, SLP_Params params) {
		
		try {
			IloCplex cp = new IloCplex();
			
			double[] pMin = new double[sys.numGens];
			double[] pMax = new double[sys.numGens];
			for (int g=0; g<sys.numGens; g++) {
				pMin[g] = sys.gens[g].pMin;
				pMax[g] = sys.gens[g].pMax;
			}
			IloNumVar[] vAngleVar = cp.numVarArray(sys.numNodes, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
			vAngleVar[0] = cp.numVar(0.0, 0.0);
			IloNumVar[] pGenVar = cp.numVarArray(sys.numGens, pMin, pMax);
			IloNumVar[] pGenLinVar = cp.numVarArray(sys.numGens*params.linearSegments, 0.0, Double.POSITIVE_INFINITY);
			IloNumVar[] pFlowVar = cp.numVarArray(sys.numLines, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
			
			IloRange[] pBalanceEqu = new IloRange[sys.numNodes];
			IloConstraint[] pFlowDefEqu = new IloRange[sys.numLines];
			
			for (int n=0; n<sys.numNodes; n++) {
				IloNumExpr pN = cp.numExpr();
				for (int g=0; g<sys.numGens; g++)
					if (sys.gens[g].node == n)
						pN = cp.sum(pN, pGenVar[g]);
				for (int l=0; l<sys.numLines; l++) {
					if (sys.lines[l].fromNode == n)
						pN = cp.diff(pN, pFlowVar[l]);
					else if (sys.lines[l].toNode == n)
						pN = cp.sum(pN, pFlowVar[l]);
				}
				pBalanceEqu[n] = cp.eq(pN, sys.nodes[n].pDemand);
			}
			
			for (int l=0; l<sys.numLines; l++) {
				pFlowDefEqu[l] = cp.eq(cp.prod(sys.lines[l].b,
											   cp.diff(vAngleVar[sys.lines[l].toNode],
													   vAngleVar[sys.lines[l].fromNode])),
									   pFlowVar[l]);
				pFlowVar[l].setLB(-sys.lines[l].i_max);
				pFlowVar[l].setUB(sys.lines[l].i_max);
			}
			
			IloRange[] pLinearSumEqu = new IloRange[sys.numGens];
			IloNumExpr cost = cp.numExpr();
			for(int g=0; g<sys.numGens; g++) {
				Generator gen = sys.gens[g];
				IloNumExpr p = pGenVar[g];
				
				for(int a=0; a<params.linearSegments; a++) {
					int ind = g*params.linearSegments+a;
					
					p = cp.sum(p, cp.negative(pGenLinVar[ind]));
					
					pGenLinVar[ind].setUB(gen.lin_size);
					cost = cp.sum(cost, cp.prod(gen.linearCost[a], pGenLinVar[ind]));		
				}
				pLinearSumEqu[g] = cp.eq(p, gen.pMin);

				cost = cp.sum(cost, gen.cMin);
			}
			
			IloObjective obj = cp.minimize(cost);
			
			cp.add(vAngleVar);
			cp.add(pGenVar);
			cp.add(pGenLinVar);
			cp.add(pFlowVar);
			
			cp.add(pBalanceEqu);
			cp.add(pFlowDefEqu);
			cp.add(pLinearSumEqu);
			
			cp.add(obj);
			
			double[] vAngle;
			if (cp.solve()) {
				System.out.printf("DC Solution found. $%.2f\n", cp.getObjValue());
				vAngle = cp.getValues(vAngleVar);
			}
			else {
				System.out.println("Could not find DC solution. Using flat start.");
				vAngle = new double[sys.numNodes];
			}
			
			
			//double[] pGen = cp.getValues(pGenVar);
			//double[] lambda = cp.getDuals(pBalanceEqu);
			cp.end();
			
			/*System.out.println("Starting point: ");
			System.out.println("Node\t| Angle\t\t| pGen\t| Lambda");
			for (int b=0; b<sys.numBus; b++) {
				System.out.printf("%d\t| %.4f\t| %.2f\t| $%.2f\n", b, vAngle[b]*180.0/Math.PI, pGen[b], lambda[b]);
			}*/
			
			return vAngle;
		}
		catch (IloException e) {
			e.printStackTrace();
			return new double[sys.numNodes];
		}
		
	}
	
	void calculateCurrents(SystemData sys) {
		for (int b=0; b < sys.numNodes; b++) {
			Node node = sys.nodes[b];
			iNodeReal[b] = node.g * vReal[b] - node.b * vImag[b];
			iNodeImag[b] = node.g * vImag[b] + node.b * vReal[b];
		}
		
		for (int l=0; l < sys.numLines; l++) {
			Branch line = sys.lines[l];
			int from = line.fromNode;
			int to = line.toNode;
			
			iLineReal[l*2] = line.g*(vReal[from]-vReal[to]) + line.b*(vImag[to]-vImag[from]) - line.b_sh*vImag[from];
			iNodeReal[from] = iNodeReal[from] + iLineReal[l*2];
			iLineImag[l*2] = line.g*(vImag[from]-vImag[to]) + line.b*(vReal[from]-vReal[to]) + line.b_sh*vReal[from];
			iNodeImag[from] = iNodeImag[from] + iLineImag[l*2];
			
			iLineReal[l*2+1] = line.g*(vReal[to]-vReal[from]) + line.b*(vImag[from]-vImag[to]) - line.b_sh*vImag[to];
			iNodeReal[to] = iNodeReal[to] + iLineReal[l*2+1];
			iLineImag[l*2+1] = line.g*(vImag[to]-vImag[from]) + line.b*(vReal[to]-vReal[from]) + line.b_sh*vReal[to];
			iNodeImag[to] = iNodeImag[to] + iLineImag[l*2+1];
		}
	}
	
	void update(IloCplex cp, IV_ACOPF_Vars vars) {
		try {
			//System.out.println("Eval point updating...");
			vReal = cp.getValues(vars.vReal);
			vImag = cp.getValues(vars.vImag);
			iLineReal = cp.getValues(vars.iLineReal);
			iLineImag = cp.getValues(vars.iLineImag);
			iNodeReal = cp.getValues(vars.iNodeReal);
			iNodeImag = cp.getValues(vars.iNodeImag);
			
			iteration++;
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	void checkVUpperBounds(SLP_Subproblem sp, SystemData sys) {
		for (int b=0; b<sys.numNodes; b++) {
			double v_sq = Math.pow(vReal[b], 2.0) + Math.pow(vImag[b], 2.0);
			if (v_sq > Math.pow(sys.nodes[b].vMax, 2.0)) {
				double scale = Math.sqrt(Math.pow(sys.nodes[b].vMax, 2.0) / v_sq);
				vReal[b] = vReal[b] * scale;
				vImag[b] = vImag[b] * scale;
				//System.out.println("Voltage at node " + b + " exceeds bounds. Adding cutting plane.");
				sp.addVCuttingPlane(sys, this, b);
			}
		}
		
	}
	
	void checkIUpperBounds (SLP_Subproblem sp, SystemData sys) {
		for (int l=0; l<sys.numLines*2; l++) {
			double ik_sq = Math.pow(iLineReal[l], 2.0) + Math.pow(iLineImag[l], 2.0);
			if (ik_sq > Math.pow(sys.lines[l/2].i_max, 2.0)) {
				double scale = Math.sqrt(Math.pow(sys.lines[l/2].i_max, 2.0) / ik_sq);
				iLineReal[l] = iLineReal[l] * scale;
				iLineImag[l] = iLineImag[l] * scale;
				//System.out.println("Current on line " + l + " exceeds bounds. Adding cutting plane.");
				sp.addICuttingPlane(sys, this, l/2, l%2);
			}
		}
	}

	/*void updateStepLimits(SLP_Subproblem sp, SystemData sys, SLP_Params params, SLP_Vars vars) {
		double f = 0.0;
		for (int b=0; b<sys.numBus; b++) {
			f += 1.0 * vars.getPowerMismatch(b);
		}
		double gamma = (vars.getPenalties() + f) / (vars.getCost() + f);
		double beta = -params.a * Math.log(gamma) + params.b;
		double delta = 1.0 - Math.floor(10.0*gamma)/10.0;
		double alpha = delta/beta;
		
		for (int b=0; b<sys.numBus; b++) {
			v_step_max[b] = alpha * sys.nodes[b].v_max / Math.pow(iter, beta);
		}
		sp.updateStepLims(sys, this);
	}*/
}
