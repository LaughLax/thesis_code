package iv_ACOPF;

public class SLP_Params {
	int maxIterations = 20;
	int linearSegments = 10;
	double a = 0.25;
	double b = 1.5;
	double r = 0.9;
	
	double iPenMultiplier = 25.0;
	double vPenMultiplier = 15.0;
	double pPenMultiplier = 2.5;
	double qPenMultiplier = 12.5;
	
	double iPenalty;
	double vPenalty;
	double pPenalty;
	double qPenalty;
	double mismatchPen = 0.2;
	
	double p_n_mismatch = 1.0e-3;
	double q_n_mismatch = 5.0e-3;
	double p_total_mismatch = 5*p_n_mismatch;
	double q_total_mismatch = 10*q_n_mismatch;
	
	public void linearizeGenCost(SystemData sys) {
		for (int g=0; g<sys.numGens; g++) {
			Generator gen = sys.gens[g];
			gen.lin_size = (gen.pMax - gen.pMin) / (float) linearSegments;
			gen.linearCost = new double[linearSegments];
			//System.out.printf("Generator %d on node %d:\n", g, gen.node);
			gen.cMin = gen.cost_2 * Math.pow(gen.pMin, 2.0)
					+ gen.cost_1 * gen.pMin
					+ gen.cost_0;
			for (int l=0; l<linearSegments; l++) {
				double low = gen.pMin + gen.lin_size*l;
				double high = gen.pMin + gen.lin_size*(l+1);
				gen.linearCost[l] = ((gen.cost_2 * Math.pow(high, 2.0))
						          - (gen.cost_2 * Math.pow(low, 2.0))) / gen.lin_size + gen.cost_1;
				//System.out.printf("Low %.1f, High %.1f, Cost %.3f\n", low, high, gen.linearCost[l]);
			}
		}
	}
	
	public void computePenalties(SystemData sys) {
		double max_c_g_l = 0;
		for (int g=0; g<sys.numGens; g++) {
			/*for (int l=0; l<linearSegments; l++) {
				max_c_g_l = Math.max(max_c_g_l, sys.gens[g].linearCost[l]);
			}*/
			max_c_g_l = Math.max(max_c_g_l, sys.gens[g].cost_1);
		}
		if (max_c_g_l == 0)
			max_c_g_l = 100;
		iPenalty = iPenMultiplier*max_c_g_l;
		vPenalty = vPenMultiplier*max_c_g_l;
		pPenalty = pPenMultiplier*max_c_g_l;
		qPenalty = qPenMultiplier*max_c_g_l;
		/*System.out.println("Ipen: " + iPenalty +
				"\t| Vpen: " + vPenalty +
				"\t| Ppen: " + pPenalty +
				"\t| Qpen: " + qPenalty);*/
	}
	
	public void increasePenalties(double multiplier) {
		iPenalty *= multiplier;
		vPenalty *= multiplier;
		pPenalty *= multiplier;
		qPenalty *= multiplier;
	}
	
}
