package iv_ACOPF;

import ilog.concert.*;
import ilog.cplex.*;

public class SLP_Subproblem {
	
	IloCplex cp;
	IV_ACOPF_Vars vars;
	EvalPoint ePoint;
	
	IloRange[] in_d_def;
	IloRange[] in_q_def;
	IloRange[] v_sq_def;
	IloRange[] p_n_def;
	IloRange[] q_n_def;
	
	IloRange[] vDStepConstraint;
	IloRange[] vQStepConstraint;
	
	IloRange[] v_sq_min_constr;
	IloRange[] v_sq_max_constr;
	
	IloRange[] ik_d_def;
	IloRange[] ik_q_def;
	IloRange[] ik_sq_def;
	IloRange[] ik_sq_constr;
	
	IloRange[] p_n_lin_constr;
	IloRange[] p_g_lin_sum;
	
	IloRange[] v_cutting_planes;
	IloRange[] ik_cutting_planes;
	
	public SLP_Subproblem(IloCplex cplex, SystemData sys, IV_ACOPF_Vars variables, SLP_Params params, EvalPoint ep) {
		try {
			initalizeVariables(sys, params);
			cp = cplex;
			vars = variables;
			ePoint = ep;
			
			IloNumExpr[] in_d = new IloNumExpr[sys.numNodes];
			IloNumExpr[] in_q = new IloNumExpr[sys.numNodes];
			IloNumExpr[] pN = new IloNumExpr[sys.numNodes];
			IloNumExpr[] qN = new IloNumExpr[sys.numNodes];
			for (int b=0; b<sys.numNodes; b++) {
				Node node = sys.nodes[b];
				
				in_d[b] = cp.sum(cp.prod(node.g, vars.vReal[b]),
								 cp.prod(-node.b, vars.vImag[b]),
								 cp.prod(-1.0, vars.iNodeReal[b]));
				in_q[b] = cp.sum(cp.prod(node.g, vars.vImag[b]),
								 cp.prod(node.b, vars.vReal[b]),
								 cp.prod(-1.0, vars.iNodeImag[b]));
				
				v_sq_def[b] = cp.eq(cp.sum(cp.sum(cp.prod(2.0*ep.vReal[b], vars.vReal[b]),
												  cp.prod(2.0*ep.vImag[b], vars.vImag[b]),
												  cp.prod(-1.0, vars.vSquared[b])),
												  - Math.pow(ep.vReal[b], 2.0)
												  - Math.pow(ep.vImag[b], 2.0)), 0.0);
				
				pN[b] = cp.sum(cp.sum(cp.prod(ep.vReal[b], vars.iNodeReal[b]),
												 cp.prod(ep.vImag[b], vars.iNodeImag[b]),
												 cp.prod(ep.iNodeReal[b], vars.vReal[b]),
												 cp.prod(ep.iNodeImag[b], vars.vImag[b]),
												 vars.pViolMinus[b],
												 cp.negative(vars.pViolPlus[b])),
												 - ep.vReal[b] * ep.iNodeReal[b]
												 - ep.vImag[b] * ep.iNodeImag[b]
												 + sys.nodes[b].pDemand);
				
				qN[b] = cp.sum(cp.sum(cp.prod(ep.vImag[b], vars.iNodeReal[b]),
												 cp.prod(-ep.vReal[b], vars.iNodeImag[b]),
												 cp.prod(ep.iNodeReal[b], vars.vImag[b]),
												 cp.prod(-ep.iNodeImag[b], vars.vReal[b]),
												 vars.qViolMinus[b],
												 cp.negative(vars.qViolPlus[b])),
												 - ep.vImag[b] * ep.iNodeReal[b]
												 + ep.vReal[b] * ep.iNodeImag[b]
												 + sys.nodes[b].qDemand);
				
				vDStepConstraint[b] = cp.range(-ep.vStepMax[b], cp.diff(vars.vReal[b], ep.vReal[b]), ep.vStepMax[b]);
				vQStepConstraint[b] = cp.range(-ep.vStepMax[b], cp.diff(vars.vImag[b], ep.vImag[b]), ep.vStepMax[b]);
				
				v_sq_min_constr[b] = cp.ge(cp.sum(vars.vSquared[b], vars.vViolMinus[b]), Math.pow(node.vMin, 2.0));
				v_sq_max_constr[b] = cp.le(cp.diff(vars.vSquared[b], vars.vViolPlus[b]), Math.pow(node.vMax, 2.0));
			}
			
			for (int g=0; g<sys.numGens; g++) {
				Generator gen = sys.gens[g];
				IloNumExpr p = cp.numExpr();
				
				for (int a=g*params.linearSegments; a<(g+1)*params.linearSegments; a++) {
					p_n_lin_constr[a] = cp.le(vars.pNodeLinear[a], gen.lin_size);
					p = cp.sum(p, cp.negative(vars.pNodeLinear[a]));
				}
				p = cp.sum(p, vars.pGen[g]);
				p_g_lin_sum[g] = cp.eq(p, gen.pMin);
				
				pN[gen.node] = cp.sum(pN[gen.node], cp.prod(-1.0, vars.pGen[g]));
				qN[gen.node] = cp.sum(qN[gen.node], cp.prod(-1.0, vars.qGen[g]));
				
			}
			
			for (int l=0; l<sys.numLines; l++) {
				Branch line = sys.lines[l];
				int from = line.fromNode;
				int to = line.toNode;
				
				IloNumExpr ik_d1 = cp.numExpr();
				ik_d1 = cp.sum(cp.prod(line.g * line.tx_Magnitude, cp.diff(cp.prod(line.tx_Magnitude, vars.vReal[from]), vars.vReal[to])),
							   cp.prod(-line.b * line.tx_Magnitude, cp.diff(cp.prod(line.tx_Magnitude, vars.vImag[from]), vars.vImag[to])),
							   cp.prod(-line.b_sh * Math.pow(line.tx_Magnitude, 2.0), vars.vImag[from]),
							   cp.prod(-1.0, vars.iLineReal[l*2]));
				in_d[from] = cp.sum(in_d[from], vars.iLineReal[l*2]);
				
				IloNumExpr ik_q1 = cp.numExpr();
				ik_q1 = cp.sum(cp.prod(line.g * line.tx_Magnitude, cp.diff(cp.prod(line.tx_Magnitude, vars.vImag[from]), vars.vImag[to])),
							   cp.prod(line.b * line.tx_Magnitude, cp.diff(cp.prod(line.tx_Magnitude, vars.vReal[from]), vars.vReal[to])),
							   cp.prod(line.b_sh * Math.pow(line.tx_Magnitude, 2.0), vars.vReal[from]),
							   cp.prod(-1.0, vars.iLineImag[l*2]));
				in_q[from] = cp.sum(in_q[from], vars.iLineImag[l*2]);

				IloNumExpr ik_d2 = cp.numExpr();
				ik_d2 = cp.sum(cp.prod(line.g, cp.diff(vars.vReal[to], cp.prod(line.tx_Magnitude, vars.vReal[from]))),
							   cp.prod(-line.b, cp.diff(vars.vImag[to], cp.prod(line.tx_Magnitude, vars.vImag[from]))),
							   cp.prod(-line.b_sh, vars.vImag[to]),
							   cp.prod(-1.0, vars.iLineReal[l*2+1]));
				in_d[to] = cp.sum(in_d[to], vars.iLineReal[l*2+1]);
				
				IloNumExpr ik_q2 = cp.numExpr();
				ik_q2 = cp.sum(cp.prod(line.g, cp.diff(vars.vImag[to], cp.prod(line.tx_Magnitude, vars.vImag[from]))),
							   cp.prod(line.b, cp.diff(vars.vReal[to], cp.prod(line.tx_Magnitude, vars.vReal[from]))),
							   cp.prod(line.b_sh, vars.vReal[to]),
							   cp.prod(-1.0, vars.iLineImag[l*2+1]));
				in_q[to] = cp.sum(in_q[to], vars.iLineImag[l*2+1]);
				
				ik_d_def[l*2] = cp.eq(ik_d1, 0.0);
				ik_q_def[l*2] = cp.eq(ik_q1, 0.0);
				ik_d_def[l*2+1] = cp.eq(ik_d2, 0.0);
				ik_q_def[l*2+1] = cp.eq(ik_q2, 0.0);
				
				ik_sq_def[l*2] = cp.eq(cp.sum(cp.sum(cp.prod(2.0*ep.iLineReal[l*2], vars.iLineReal[l*2]),
													 cp.prod(2.0*ep.iLineImag[l*2], vars.iLineImag[l*2]),
													 cp.prod(-1.0, vars.iLineSquared[l*2])),
													 - Math.pow(ep.iLineReal[l*2], 2.0)
													 - Math.pow(ep.iLineImag[l*2], 2.0)), 0.0);
				
				ik_sq_def[l*2+1] = cp.eq(cp.sum(cp.sum(cp.prod(2.0*ep.iLineReal[l*2+1], vars.iLineReal[l*2+1]),
						 							   cp.prod(2.0*ep.iLineImag[l*2+1], vars.iLineImag[l*2+1]),
						 							   cp.prod(-1.0, vars.iLineSquared[l*2+1])),
						 							   - Math.pow(ep.iLineReal[l*2+1], 2.0)
						 							   - Math.pow(ep.iLineImag[l*2+1], 2.0)), 0.0);
				
				ik_sq_constr[l*2] = cp.le(cp.diff(vars.iLineSquared[l*2], vars.iViolPlus[l*2]), Math.pow(line.i_max, 2.0));
				ik_sq_constr[l*2+1] = cp.le(cp.diff(vars.iLineSquared[l*2+1], vars.iViolPlus[l*2+1]), Math.pow(line.i_max, 2.0));
			}
			for (int b=0; b<sys.numNodes; b++) {
				p_n_def[b] = cp.eq(pN[b], 0.0);
				q_n_def[b] = cp.eq(qN[b], 0.0);
				
				in_d_def[b] = cp.eq(in_d[b], 0.0);
				in_q_def[b] = cp.eq(in_q[b], 0.0);
			}
			
			addToModel();
		}
		catch(IloException e) {
			e.printStackTrace();
		}
	}
	
	private void initalizeVariables(SystemData sys, SLP_Params params) {
		in_d_def = new IloRange[sys.numNodes];
		in_q_def = new IloRange[sys.numNodes];
		v_sq_def = new IloRange[sys.numNodes];
		p_n_def = new IloRange[sys.numNodes];
		q_n_def = new IloRange[sys.numNodes];
		
		vDStepConstraint = new IloRange[sys.numNodes];
		vQStepConstraint = new IloRange[sys.numNodes];
		
		v_sq_min_constr = new IloRange[sys.numNodes];
		v_sq_max_constr = new IloRange[sys.numNodes];
		
		p_n_lin_constr = new IloRange[sys.numGens*params.linearSegments];
		p_g_lin_sum = new IloRange[sys.numGens];
		
		ik_d_def = new IloRange[sys.numLines*2];
		ik_q_def = new IloRange[sys.numLines*2];
		ik_sq_def = new IloRange[sys.numLines*2];
		ik_sq_constr = new IloRange[sys.numLines*2];
		
		v_cutting_planes = new IloRange[0];
		ik_cutting_planes = new IloRange[0];
	}
	
	private void addToModel() {
		try {
			cp.add(vars.objFunction);
			
			cp.add(v_sq_def);
			cp.add(p_n_def);
			cp.add(q_n_def);
			
			cp.add(vDStepConstraint);
			cp.add(vQStepConstraint);
			
			cp.add(v_sq_min_constr);
			cp.add(v_sq_max_constr);
			
			cp.add(p_g_lin_sum);
			cp.add(p_n_lin_constr);

			cp.add(in_d_def);
			cp.add(in_q_def);
			cp.add(ik_d_def);
			cp.add(ik_q_def);
			cp.add(ik_sq_def);
			cp.add(ik_sq_constr);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public int run() {
		try{
			if (cp.solve()) {
				vars.getRunResult();

				System.out.printf("Subproblem %d solution status = %s ($%.2f, $%.2f penalties)\n",
						ePoint.iteration,
						cp.getStatus(),
						cp.getObjValue(),
						vars.penaltyValue);
				
				return 1;
			}
			else {
				System.err.println("Subproblem failed to solve. Quitting.");
				return 0;
			}
		}
		catch(IloException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public void updateEvalPoint(SystemData sys, EvalPoint ep) {
		try {
			cp.remove(v_sq_def);
			cp.remove(p_n_def);
			cp.remove(q_n_def);
			cp.remove(ik_sq_def);
			
			IloNumExpr[] pN = new IloNumExpr[sys.numNodes];
			IloNumExpr[] qN = new IloNumExpr[sys.numNodes];
			
			for (int b=0; b<sys.numNodes; b++) {
				v_sq_def[b] = cp.eq(cp.sum(cp.sum(cp.prod(2.0*ep.vReal[b], vars.vReal[b]),
						cp.prod(2.0*ep.vImag[b], vars.vImag[b]),
						cp.prod(-1.0, vars.vSquared[b])),
						- Math.pow(ep.vReal[b], 2.0)
						- Math.pow(ep.vImag[b], 2.0)), 0.0);
				
				pN[b] = cp.sum(cp.sum(cp.prod(ep.vReal[b], vars.iNodeReal[b]),
						cp.prod(ep.vImag[b], vars.iNodeImag[b]),
						cp.prod(ep.iNodeReal[b], vars.vReal[b]),
						cp.prod(ep.iNodeImag[b], vars.vImag[b]),
						vars.pViolMinus[b],
						cp.negative(vars.pViolPlus[b])),
						- ep.vReal[b] * ep.iNodeReal[b]
						- ep.vImag[b] * ep.iNodeImag[b]
						+ sys.nodes[b].pDemand);
				
				qN[b] = cp.sum(cp.sum(cp.prod(ep.vImag[b], vars.iNodeReal[b]),
						cp.prod(-ep.vReal[b], vars.iNodeImag[b]),
						cp.prod(ep.iNodeReal[b], vars.vImag[b]),
						cp.prod(-ep.iNodeImag[b], vars.vReal[b]),
						vars.qViolMinus[b],
						cp.negative(vars.qViolPlus[b])),
						- ep.vImag[b] * ep.iNodeReal[b]
						+ ep.vReal[b] * ep.iNodeImag[b]
						+ sys.nodes[b].qDemand);
			}
			cp.add(v_sq_def);
			
			for (int g=0; g<sys.numGens; g++) {
				Generator gen = sys.gens[g];
				pN[gen.node] = cp.sum(pN[gen.node], cp.prod(-1.0, vars.pGen[g]));
				qN[gen.node] = cp.sum(qN[gen.node], cp.prod(-1.0, vars.qGen[g]));
			}
			
			for (int b=0; b<sys.numNodes; b++) {
				p_n_def[b] = cp.eq(pN[b], 0.0);
				q_n_def[b] = cp.eq(qN[b], 0.0);
			}
			
			cp.add(p_n_def);
			cp.add(q_n_def);
			
			for (int l=0; l<sys.numLines; l++) {
				ik_sq_def[l*2] = cp.eq(cp.sum(cp.sum(cp.prod(2.0*ep.iLineReal[l*2], vars.iLineReal[l*2]),
						cp.prod(2.0*ep.iLineImag[l*2], vars.iLineImag[l*2]),
						cp.prod(-1.0, vars.iLineSquared[l*2])),
						- Math.pow(ep.iLineReal[l*2], 2.0)
						- Math.pow(ep.iLineImag[l*2], 2.0)), 0.0);

				ik_sq_def[l*2+1] = cp.eq(cp.sum(cp.sum(cp.prod(2.0*ep.iLineReal[l*2+1], vars.iLineReal[l*2+1]),
						cp.prod(2.0*ep.iLineImag[l*2+1], vars.iLineImag[l*2+1]),
						cp.prod(-1.0, vars.iLineSquared[l*2+1])),
						- Math.pow(ep.iLineReal[l*2+1], 2.0)
						- Math.pow(ep.iLineImag[l*2+1], 2.0)), 0.0);
			}
			cp.add(ik_sq_def);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}

	public void addVCuttingPlane(SystemData sys, EvalPoint ep, int node) {
		try {
			int len = v_cutting_planes.length;
			IloRange[] v = new IloRange[len+1];
			for (int i=0; i<len; i++) {
				v[i] = v_cutting_planes[i];
			}
			v[len] = cp.le(cp.sum(cp.prod(ep.vReal[node], vars.vReal[node]),
								  cp.prod(ep.vImag[node], vars.vImag[node]),
								  cp.prod(-1.0, vars.vViolPlus[node])),
					       Math.pow(sys.nodes[node].vMax, 2.0));
			cp.add(v[len]);
			
			v_cutting_planes = v;
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void addICuttingPlane(SystemData sys, EvalPoint ep, int l, int dir) {
		try {
			int len = ik_cutting_planes.length;
			IloRange[] ik = new IloRange[len+1];
			for (int i=0; i<len; i++) {
				ik[i] = ik_cutting_planes[i];
			}
			ik[len] = cp.le(cp.sum(cp.prod(ep.iLineReal[l*2+dir], vars.iLineReal[l*2+dir]),
	    			   			   cp.prod(ep.iLineImag[l*2+dir], vars.iLineImag[l*2+dir]),
	    			   			   cp.prod(-1.0, vars.iViolPlus[l*2+dir])), 
						    Math.pow(sys.lines[l].i_max, 2.0));
			
			//cp.add(ik[len]);

			ik_cutting_planes = ik;
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public void updateFlowConstraints(IloCplex cp, SystemData sys, SLP_Params params, EvalPoint ep) {
		try {
			cp.remove(ik_sq_def);
			cp.remove(ik_sq_constr);
			
			for (int l=0; l<sys.numLines; l++) {
				double i1 = Math.pow(ep.iLineReal[l*2], 2.0) + Math.pow(ep.iLineImag[l*2], 2.0);
				double i2 = Math.pow(ep.iLineReal[l*2+1], 2.0) + Math.pow(ep.iLineImag[l*2+1], 2.0);
				if ((i1 >= Math.pow(params.r * sys.lines[l].i_max, 2.0)) 
						|| (i2 >= Math.pow(params.r * sys.lines[l].i_max, 2.0))) {
					//System.out.println("Leaving flowgate monitor on line " + l);
					cp.add(ik_sq_def[l*2]);
					cp.add(ik_sq_def[l*2+1]);
					cp.add(ik_sq_constr[l*2]);
					cp.add(ik_sq_constr[l*2+1]);
				}
			}
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}

	public void updateStepLims(SystemData sys, SLP_Params params, EvalPoint ep) {
		try {
			double f = 0.0;
			for (int b=0; b<sys.numNodes; b++) {
				f += params.mismatchPen * vars.pMismatch[b];
			}
			double gamma = (vars.penaltyValue + f) / (vars.costValue + f);
			gamma = Math.min(gamma, 1);
			double beta = -params.a * Math.log(gamma) + params.b;
			double delta = 1.0 - Math.floor(10.0*gamma)/10.0;
			double alpha = beta/delta;
			
			for (int b=0; b<sys.numNodes; b++) {
				ep.vStepMax[b] = alpha * sys.nodes[b].vMax / Math.pow(ep.iteration, beta);
			}
			
			cp.remove(vDStepConstraint);
			cp.remove(vQStepConstraint);
			
			for (int b=0; b<sys.numNodes; b++) {
				vDStepConstraint[b] = cp.range(-ep.vStepMax[b], cp.diff(vars.vReal[b], ep.vReal[b]), ep.vStepMax[b]);
				vQStepConstraint[b] = cp.range(-ep.vStepMax[b], cp.diff(vars.vImag[b], ep.vImag[b]), ep.vStepMax[b]);
			}
			
			cp.add(vDStepConstraint);
			cp.add(vQStepConstraint);
		}
		catch (IloException e) {
			e.printStackTrace();
		}		
	}
	
	public void tinyStepLims(EvalPoint ep) {
		try {
			
			cp.remove(vDStepConstraint);
			cp.remove(vQStepConstraint);
			
			for (int b=0; b<vDStepConstraint.length; b++) {
				
				vDStepConstraint[b] = cp.range(-1e-10, cp.diff(vars.vReal[b], ep.vReal[b]), 1e-10);
				vQStepConstraint[b] = cp.range(-1e-10, cp.diff(vars.vImag[b], ep.vImag[b]), 1e-10);
			}
			cp.add(vDStepConstraint);
			cp.add(vQStepConstraint);
		}
		catch (IloException e) {
			e.printStackTrace();
		}
	}
	
	public int runWithNoPenalties(SystemData sys, EvalPoint ep) {
		try {
			ep.update(cp, vars);
			updateEvalPoint(sys, ep);
			
			cp.remove(vars.objFunction);
			cp.addMinimize(vars.costExpression);
			
			tinyStepLims(ep);
			
			if (cp.solve()) {
				System.out.println("Final run successful");
				return 1;
			}
			else {
				System.out.println("Final run failed - " + cp.getStatus());
				return 0;
			}
			
		}
		catch (IloException e) {
			e.printStackTrace();
			return 0;
		}
	}
}
