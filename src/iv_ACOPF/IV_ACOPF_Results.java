package iv_ACOPF;

import ilog.cplex.*;
import ilog.concert.*;

public class IV_ACOPF_Results {
	
	SystemData sys;
	IV_ACOPF_Vars vars;
	
	double objectiveValue;
	double[] vReal;
	double[] vImag;
	double[] iLineReal;
	double[] iLineImag;
	double[] lambda;
	
	public IV_ACOPF_Results(IloCplex cp, IV_ACOPF_Vars variables, SLP_Subproblem sp, SystemData sysData) {
		try {
			objectiveValue = cp.getObjValue();
			
			vars = variables;
			sys = sysData;
			
			vReal = cp.getValues(vars.vReal);
			vImag = cp.getValues(vars.vImag);
			iLineReal = cp.getValues(vars.iLineReal);
			iLineImag = cp.getValues(vars.iLineImag);
			lambda = cp.getDuals(sp.p_n_def);
		}
		catch (IloException e) {
			
		}
	}
	
	void printSummary() {
		System.out.printf("Objective function value: $%.2f\n", objectiveValue);
	}
	
	void printResults() {
		if (sys.numNodes <= 100) {
			System.out.println("Node\t| Voltage mag.\t| V angle\t| Pg\t| Qg\t| Pd\t| Qd\t| LMP\t| (#,to) P/Q");
			for (int b=0; b<sys.numNodes; b++) {
				double v_mag = Math.sqrt(Math.pow(vReal[b],2.0) + Math.pow(vImag[b],2.0));
				double v_ang = Math.atan2(vImag[b], vReal[b]) * 180 / Math.PI;
				lambda[b] = Math.abs(lambda[b]);
				System.out.printf("%d\t| %.4f\t| %.4f\t| %.1f\t| %.1f\t| %.1f\t| %.1f\t| $%.2f",
						b,
						v_mag,
						v_ang,
						vars.pActual[b],
						vars.qActual[b],
						sys.nodes[b].pDemand,
						sys.nodes[b].qDemand,
						lambda[b]);
				for (int l=0; l<sys.numLines; l++) {
					if (sys.lines[l].fromNode == b) {
						int to = sys.lines[l].toNode;
						System.out.printf("\t| (%d,%d) %.1f/%.1f",
								l, to,
								vReal[b] * iLineReal[l*2] + vImag[b] * iLineImag[l*2],
								vImag[b] * iLineReal[l*2] - vReal[b] * iLineImag[l*2]);
					}
					if (sys.lines[l].toNode == b) {
						int to = sys.lines[l].fromNode;
						System.out.printf("\t| (%d,%d) %.1f/%.1f",
								l, to,
								vReal[b] * iLineReal[l*2+1] + vImag[b] * iLineImag[l*2+1],
								vImag[b] * iLineReal[l*2+1] - vReal[b] * iLineImag[l*2+1]);
					}
				}
				System.out.println();
			}
			
			System.out.println("\nBranch\t| From\t| To\t| Forward I\t| Forward P\t| Forward Q\t| Reverse I\t| Reverse P\t| Reverse Q");
			//System.out.println("\nBranch\t| From\t| To\t| Forward I\t\t| Reverse I");
			for (int l=0; l<sys.numLines; l++) {
				int from = sys.lines[l].fromNode;
				int to = sys.lines[l].toNode;
				double ikMag1 = Math.signum(iLineReal[l*2]) * Math.sqrt(Math.pow(iLineReal[l*2], 2.0) + Math.pow(iLineImag[l*2], 2.0));
				//double ikAng1 = 180/Math.PI * Math.atan(iLineImag[l*2]/iLineReal[l*2]);
				double ikMag2 = Math.signum(iLineReal[l*2+1]) * Math.sqrt(Math.pow(iLineReal[l*2+1], 2.0) + Math.pow(iLineImag[l*2+1], 2.0));
				//double ikAng2 = 180/Math.PI * Math.atan(iLineImag[l*2+1]/iLineReal[l*2+1]);
				double pFlow1 = vReal[from] * iLineReal[l*2] + vImag[from] * iLineImag[l*2];
				double qFlow1 = vImag[from] * iLineReal[l*2] - vReal[from] * iLineImag[l*2];
				double pFlow2 = vReal[to] * iLineReal[l*2+1] + vImag[to] * iLineImag[l*2+1];
				double qFlow2 = vImag[to] * iLineReal[l*2+1] - vReal[to] * iLineImag[l*2+1];
				if (sys.lines[l].i_max != Double.POSITIVE_INFINITY)
					System.out.printf("%d\t| %d\t| %d\t| %.2f/%.2f\t| %.4f\t| %.4f\t| %.2f/%.2f\t| %.4f\t| %.4f\t| %.4e/%.4e/%.4e\n",
							l,
							from,
							to,
							ikMag1,sys.lines[l].i_max,
							pFlow1,
							qFlow1,
							ikMag2,sys.lines[l].i_max,
							pFlow2,
							qFlow2,
							sys.lines[l].r,
							sys.lines[l].x,
							sys.lines[l].b_sh);
					/*System.out.printf("%d\t| %d\t| %d\t| %.2f/%.2f\t| %.2f/%.2f\n",
							l, from, to,
							ikMag1, ikAng1,
							ikMag2, ikAng2);*/
				else
					System.out.printf("%d\t| %d\t| %d\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4f\t| %.4e/%.4e/%.4e\n",
							l,
							from,
							to,
							ikMag1,
							pFlow1,
							qFlow1,
							ikMag2,
							pFlow2,
							qFlow2,
							sys.lines[l].r,
							sys.lines[l].x,
							sys.lines[l].b_sh);
					/*System.out.printf("%d\t| %d\t| %d\t| %.4f/%.4f\t| %.4f/%.4f\n",
							l, from, to,
							ikMag1, ikAng1,
							ikMag2, ikAng2);*/
			}
		}
	}
}
