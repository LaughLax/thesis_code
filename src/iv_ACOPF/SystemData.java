package iv_ACOPF;
/*Copyright (c) 2013, Mostafa Sahraei-Ardakani, Arizona State University 
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
�	Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;


public class SystemData {
	//	 int th_id, nthreads;
	int numScen;
	int numPeriods;
	int maxSwitching;
	int listLength;
	int numLines;
	int numGens;
	int numNodes;
	double[][] Schedulep = new double [numGens][numPeriods];
	boolean[][] Scheduleu = new boolean [numGens][numPeriods];
	boolean[] gen_stat = new boolean [numGens];
	boolean[] line_stat = new boolean [numLines];	
	Generator[] gens;
	Node[] nodes;
	Branch[] lines;
	TimePeriod[] periods;

	public void initialize(){
		gens = new Generator[numGens];
		nodes = new Node[numNodes];
		lines = new Branch[numLines];
		periods = new TimePeriod[numPeriods];

		for (int i=0; i<numGens; i++) {
			gens[i]=new Generator();
		}
		for (int i=0; i<numNodes; i++) {
			nodes[i]=new Node();
		}
		for (int i=0; i<numLines; i++) {
			lines[i]=new Branch();
		}
		for (int i=0; i<numPeriods; i++) {
			periods[i]=new TimePeriod();
		}
	}

	public void loadParams(String folderName) {
		
		String rootDr=(System.getProperty("user.dir")+"/cases/"+folderName+"/");
		
		try {
			InputStream myFile= new FileInputStream(rootDr+"modelParameters.txt");
			Scanner modelParam = new Scanner(myFile);
			
			numGens=modelParam.nextInt();
			numNodes=modelParam.nextInt();
			numLines=modelParam.nextInt();
			numScen=modelParam.nextInt();
			listLength=modelParam.nextInt();
			numPeriods=modelParam.nextInt();
			maxSwitching=modelParam.nextInt();
			
			System.out.println("Model Parameters Was Successfully Read.");
			modelParam.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error loading Parameters file!");
			e.printStackTrace();
		}
		//System.out.println(numPeriod);
		//public SchpInfo schedulep [][]=new SchpInfo [numPeriod][numGen]; 

	}	 

	public void loadData(String folderName) {

		String rootDr=(System.getProperty("user.dir")+"/cases/"+folderName+"/");

		if (numPeriods > 1) {
			try {
				InputStream myFile = new FileInputStream(rootDr+"periods.txt");
				Scanner periodData = new Scanner(myFile);
				for (int t=0; t<numPeriods; ++t){
					periods[t].hour=periodData.nextInt();
					periods[t].loadPercent=periodData.nextDouble();
				}

				System.out.println("Period Data Was Successfully Read.");
				periodData.close();
				myFile.close();
			}
			catch (Exception e) {
				System.out.println("Error loading period file! " + e);
				e.printStackTrace();
			}
		}

		/*
				try
				{

					InputStream myFile2= new FileInputStream(rootDr+"ScheduleUCp.txt");
					Scanner SchedDataP = new Scanner(myFile2);
					for (int t=0; t<numPeriod; ++t){
						for(int g=0; g<numGen; ++g){
							Schedulep[g][t]=SchedDataP.nextDouble();
							//System.out.println(g+" "+t);

						}
					}

					System.out.println("Schedule Data Was Successfully Read.");
					SchedDataP.close();
					myFile2.close();
				}
				catch (Exception Err2)
				{
					System.out.println();
					System.out.println("Error Loading Scuedule File " +Err2);
					System.out.println();

				}

				try
				{
					InputStream myFile3= new FileInputStream(rootDr+"ScheduleUCu.txt");
					Scanner SchedDataU = new Scanner(myFile3);
					for (int t=0; t<numPeriod; ++t){
						for(int g=0; g<numGen; ++g){
							//System.out.println(g+" "+t);
							int tmp=SchedDataU.nextInt();
							if (tmp==0)
								{Scheduleu[g][t]=false;}
							else
								{Scheduleu[g][t]=true;}
						}
					}

					System.out.println("Schedule Data Was Successfully Read.");
					SchedDataU.close();
					myFile3.close();
				}
				catch (Exception Err3)
				{
					System.out.println();
					System.out.println("Error Loading Schedule File "+ Err3);
					System.out.println();

				} */

		try {
			InputStream myFile = new FileInputStream(rootDr+"nodes.txt");
			Scanner NodeData = new Scanner(myFile);
			for(int n=0; n<numNodes; n++){
				nodes[n].id=NodeData.nextInt()-1;
				nodes[n].pDemand=NodeData.nextDouble();
				nodes[n].qDemand=NodeData.nextDouble();
				nodes[n].g=NodeData.nextDouble();
				nodes[n].b=NodeData.nextDouble();
				nodes[n].vMin=NodeData.nextDouble();
				nodes[n].vMax=NodeData.nextDouble();
			}	

			System.out.println("Node Data Was Successfully Read.");
			NodeData.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Node File!");
			e.printStackTrace();
		}

		try {
			InputStream myFile = new FileInputStream(rootDr+"generators.txt");
			Scanner GenData = new Scanner(myFile);

			for(int g=0; g<numGens; ++g){
				gens[g].id=GenData.nextInt()-1;
				int n = GenData.nextInt()-1;
				gens[g].node= n;
				nodes[n].hasGen = true;
				gens[g].pMin=GenData.nextDouble();
				gens[g].pMax=GenData.nextDouble();
				gens[g].qMin=GenData.nextDouble();
				gens[g].qMax=GenData.nextDouble();
				gens[g].cost_2=GenData.nextDouble();
				gens[g].cost_1=GenData.nextDouble();
				gens[g].cost_0=GenData.nextDouble();
				gens[g].rampRate=GenData.nextDouble();
				gens[g].status=GenData.nextInt();
			}

			System.out.println("Gen Data Was Successfully Read.");
			GenData.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Gen File!");
			e.printStackTrace();
		}

		try{
			InputStream myFile= new FileInputStream(rootDr+"lines.txt");
			Scanner LineData = new Scanner(myFile);
			for(int l=0; l<numLines; ++l){
				// Read in raw data
				lines[l].id=LineData.nextInt()-1;
				lines[l].fromNode=LineData.nextInt()-1;
				lines[l].toNode=LineData.nextInt()-1;
				lines[l].r=LineData.nextDouble();
				lines[l].x=LineData.nextDouble();
				lines[l].b_sh=LineData.nextDouble() / 2.0;
				lines[l].i_max=LineData.nextDouble();
				lines[l].tx_Magnitude=LineData.nextDouble();
				lines[l].tx_Phase=LineData.nextDouble();
				if (lines[l].tx_Phase != 0.0)
					System.err.println("Phase shifter in the system! Line " +l);
				lines[l].status=LineData.nextInt();

				// Clean up data as needed
				if (lines[l].i_max == 0.0)
					lines[l].i_max = Double.POSITIVE_INFINITY;

				if (lines[l].tx_Magnitude == 0.0)
					lines[l].tx_Magnitude = 1.0;
				else
					lines[l].tx_Magnitude = 1/lines[l].tx_Magnitude;

				// Calculate g and b
				double scale = Math.pow(lines[l].r, 2.0) + Math.pow(lines[l].x, 2.0);
				lines[l].g = lines[l].r / scale;
				lines[l].b = -lines[l].x / scale;
			}

			System.out.println("Line Data Was Successfully Read.");
			LineData.close();
			myFile.close();
		}
		catch (Exception e) {
			System.out.println("Error Loading Line File!");
			e.printStackTrace();
		}

		System.out.println();
		System.out.println("Data Load Finished.");
	}

	public void adjustLineLimits(double multiplier) {
		for (int l=0; l<numLines; l++) {
			lines[l].i_max *= multiplier;
		}
	}

	public void adjustQLimits(double multiplier) {
		System.out.println("Adjusting Q Limits");
		for (int g=0; g<numGens; g++) {
			int b = gens[g].node;
			System.out.printf("Gen %d, Node %d, gQ %.1f/%.1f, ",
					g,b,
					gens[g].qMin, gens[g].qMax);
			gens[g].qMin *= multiplier;
			gens[g].qMax *= multiplier;
			System.out.printf("gQ %.1f/%.1f\n",
					gens[g].qMin, gens[g].qMax);
		}
	}
}